import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Chirp\\Argentina 2018 Wednesday Demo On-track Afternoon.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [15.52,
                        62.31,
                        108.45,
                        144.10,
                        182.93]

time_crop_left_list = [42.18,
                       87.89,
                       125.31,
                       160.65,
                       200.25]

#Create the threshold data
chirp_1 = (df_raw_constant_radius['Time'] > 15) & (df_raw_constant_radius['Time'] <= 42)
chirp_2 = (df_raw_constant_radius['Time'] > 62) & (df_raw_constant_radius['Time'] <= 87)
chirp_3 = (df_raw_constant_radius['Time'] > 108) & (df_raw_constant_radius['Time'] <= 125)
chirp_4 = (df_raw_constant_radius['Time'] > 144) & (df_raw_constant_radius['Time'] <= 160)
chirp_5 = (df_raw_constant_radius['Time'] > 182) & (df_raw_constant_radius['Time'] <= 200)



#Get the data frame data with the treshold
#Note here the data was recorded the other way around

df_chirp_1 = df_raw_constant_radius[chirp_1]
df_chirp_2 = df_raw_constant_radius[chirp_2]
df_chirp_3 = df_raw_constant_radius[chirp_3]
df_chirp_4 = df_raw_constant_radius[chirp_4]
df_chirp_5 = df_raw_constant_radius[chirp_5]



fig, axs = plt.subplots(5)
axs[0].plot(df_chirp_1.loc[:,"Time"], df_chirp_1.loc[:,"Steered Angle"])
axs[1].plot(df_chirp_2.loc[:,"Time"], df_chirp_2.loc[:,"Steered Angle"])
axs[2].plot(df_chirp_3.loc[:,"Time"], df_chirp_3.loc[:,"Steered Angle"])
axs[3].plot(df_chirp_4.loc[:,"Time"], df_chirp_4.loc[:,"Steered Angle"])
axs[4].plot(df_chirp_5.loc[:,"Time"], df_chirp_5.loc[:,"Steered Angle"])



axs[0].legend(['Chirp 1'])
axs[1].legend(['Chirp 2'])
axs[2].legend(['Chirp 3'])
axs[3].legend(['Chirp 4'])
axs[4].legend(['Chirp 5'])



#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_chirp_1.loc[:,"Time"] - df_chirp_1.loc[df_chirp_1["Time"].index[0],"Time"], df_chirp_1.loc[:,"Steered Angle"])
ax5.plot(df_chirp_2.loc[:,"Time"] - df_chirp_2.loc[df_chirp_2["Time"].index[0],"Time"], df_chirp_2.loc[:,"Steered Angle"])
ax5.plot(df_chirp_3.loc[:,"Time"] - df_chirp_3.loc[df_chirp_3["Time"].index[0],"Time"], df_chirp_3.loc[:,"Steered Angle"])
ax5.plot(df_chirp_4.loc[:,"Time"] - df_chirp_4.loc[df_chirp_4["Time"].index[0],"Time"], df_chirp_4.loc[:,"Steered Angle"])
ax5.plot(df_chirp_5.loc[:,"Time"] - df_chirp_5.loc[df_chirp_5["Time"].index[0],"Time"], df_chirp_5.loc[:,"Steered Angle"])



ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Chirp")
ax5.legend(['Chirp 1',"Chirp 2", "Chirp  3", "Chirp 4","Chirp 5", "Chirp 6"])
ax5.grid()

