"""
Joao Antunes
Calculate the understeer grandient for each of the tires
"""
import numpy as np
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal


# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_tests_spec1 = import_csv("sunday_skidpad_spec1")
df_tests_spec2 = import_csv("monday_skidpad_spec2")

# Select test to be plotted.
selected_data_spec1 = df_tests_spec1[0]
selected_data_spec2 = df_tests_spec2[3]

selected_data_spec1["SAS2 Velocity X"] = selected_data_spec1["SAS3 S350 Velocity X"]
selected_data_spec1["SAS2 Velocity Y"] = selected_data_spec1["SAS3 S350 Velocity Y"]

selected_datas = [selected_data_spec1, selected_data_spec2]
########################################################################################################################
min_threshold_1 = 5
max_threshold_1 = 250

# Define variable
x_time_list = []
vehicle_speed_list = []
beta_list = []
lateral_acceleration_list = []
for selected_data in selected_datas:
    x_time_list.append(selected_data["Time"].loc[np.abs((selected_data["Vehicle Speed"]) > min_threshold_1) & (selected_data["Vehicle Speed"]<max_threshold_1)])
    vehicle_speed_list.append(np.abs(selected_data["Vehicle Speed"].loc[np.abs((selected_data["Vehicle Speed"]) > min_threshold_1) & (selected_data["Vehicle Speed"]<max_threshold_1)]))
    beta_x = np.abs(selected_data["SAS2 Velocity X"].loc[np.abs((selected_data["Vehicle Speed"]) > min_threshold_1) & (selected_data["Vehicle Speed"]<max_threshold_1)])
    beta_y = np.abs(selected_data["SAS2 Velocity Y"].loc[np.abs((selected_data["Vehicle Speed"]) > min_threshold_1) & (selected_data["Vehicle Speed"]<max_threshold_1)])
    beta_list.append(beta_y/beta_x)


# Define variable
f_beta_list = []
for i in range(len(beta_list)):
   b = signal.medfilt(beta_list[i], 201)
   f_beta_list.append(b)


#Data Inspection:
# Side Slip Angle - Raw Data
_, ax = plt.subplots(2)
for i in range(len(x_time_list)):
    ax[0].plot(x_time_list[i], beta_list[i], color="darkred")
    ax[1].plot(x_time_list[i], beta_list[i], color="darkgreen")

#ax.legend(["Tire A", "Tire B"])
plt.xlabel("Vehicle Speed [Km/h]")
plt.ylabel("Side Slip Angle [deg]")
plt.title("Side Slip Angle")
plt.grid(True)


########################################################################################################################
# Side Slip Angle - Raw Data
_, ax = plt.subplots()
for i in range(len(x_time_list)):
    ax.plot(vehicle_speed_list[i], beta_list[i], color=GlobalTestFile.color_pallete_dark[i])

#ax.legend(["Tire A", "Tire B"])
plt.xlabel("Vehicle Speed [Km/h]")
plt.ylabel("Side Slip Angle [deg]")
plt.title("Side Slip Angle")
plt.grid(True)



########################################################################################################################
# Side Slip Angle - Filtered Data
_, ax = plt.subplots()
for i in range(len(x_time_list)):
    ax.scatter(vehicle_speed_list[i], f_beta_list[i], color=GlobalTestFile.color_pallete_dark[i])

#ax.legend(["Tire A", "Tire B"])
plt.xlabel("Vehicle Speed [Km/h]")
plt.ylabel("Side Slip Angle [deg]")
plt.title("Side Slip Angle")
plt.grid(True)

########################################################################################################################
# Fit curve to data

_, ax = plt.subplots()
for i in range(len(x_time_list)):
    try:
        fitted_data = np.polyfit(vehicle_speed_list[i], f_beta_list[i], 3)
        min_lin = np.min(vehicle_speed_list[i])
        max_lin = np.max(vehicle_speed_list[i])
        xp = np.linspace(min_lin, max_lin, 100)
        p_1 = np.poly1d(fitted_data)
        ax.scatter(vehicle_speed_list[i], f_beta_list[i], c=GlobalTestFile.color_pallete_dark[i], edgecolors=None)
        ax.plot(xp, p_1(xp), c=GlobalTestFile.color_pallete_light[i], linewidth=3.0)
        ax.legend(["Tire A", "Tire B"])
        plt.xlabel("Vehicle Speed [Km/h]")
        plt.ylabel("Side Slip Angle [deg]")
        plt.grid(True)
        plt.draw()
        plt.pause(0.1)
    except:
        print("It was not possible to fit the data of test: " + str(GlobalTestFile.TEST_NUMBER))


if __name__ == "__main__":
    plt.show()
