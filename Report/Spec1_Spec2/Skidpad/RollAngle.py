"""
Joao Antunes
Plot's the Slip Angles and compares with the forces and temperatures.
"""
import numpy as np
import pandas as pd
import forces
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal


# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_tests_spec1 = import_csv("sunday_skidpad_spec1")
df_tests_spec2 = import_csv("monday_skidpad_spec2")

# Select test to be plotted.
selected_data_spec1 = df_tests_spec1[2]
selected_data_spec2 = df_tests_spec2[3]

selected_datas = [selected_data_spec1, selected_data_spec2]
########################################################################################################################
# Define variable
x_time_list = []
vehicle_speed_list = []
roll_angle_list = []
lateral_acceleration_list = []
for selected_data in selected_datas:

    x_time_list.append(selected_data.loc[:, "Time"])
    vehicle_speed_list.append(selected_data.loc[:, "Vehicle Speed"])
    roll_angle_list.append(selected_data.loc[:, "ADMA Roll Angle"])
    lateral_acceleration_list.append(selected_data.loc[:, "ADMA Accel Body Y"])

# Conversion
for i in range(len(lateral_acceleration_list)):
    lateral_acceleration_list[i] = np.abs(lateral_acceleration_list[i])

# Pre-Processing
# Roll Angle should start at zero
for i in range(len(roll_angle_list)):
    roll_angle_list[i] = np.abs(np.min(roll_angle_list[i])) - roll_angle_list[i]

for i in range(len(x_time_list)):
    x_time_list[i] = x_time_list[i].loc[lateral_acceleration_list[i] > 0.2]
    x_time_list[i] = x_time_list[i].loc[lateral_acceleration_list[i] < 0.9]
    roll_angle_list[i] = roll_angle_list[i].loc[lateral_acceleration_list[i] > 0.2]
    roll_angle_list[i] = roll_angle_list[i].loc[lateral_acceleration_list[i] < 0.9]
    lateral_acceleration_list[i] = lateral_acceleration_list[i].loc[lateral_acceleration_list[i] > 0.2]
    lateral_acceleration_list[i] = lateral_acceleration_list[i].loc[lateral_acceleration_list[i] < 0.9]



# Filter the data
# Downsample
for i in range(len(x_time_list)):
    roll_angle_list[i] = signal.decimate(roll_angle_list[i], 10)
    x_time_list[i] = signal.decimate(x_time_list[i], 10)
    lateral_acceleration_list[i] = signal.decimate(lateral_acceleration_list[i], 10)



for i in range(len(x_time_list)):
    roll_angle_list[i] = signal.medfilt(roll_angle_list[i], 31)


########################################################################################################################
# Plot the raw data of Vehicle speed and lateral acceleration
_, ax = plt.subplots()
for i in range(len(x_time_list)):

    ax.scatter(x_time_list[i], roll_angle_list[i])
    plt.xlabel("Time [s]")
    plt.ylabel("Roll Angle [deg]")
    plt.grid(True)

########################################################################################################################
# Plot the raw data of Vehicle speed and lateral acceleration
_, ax = plt.subplots()
for i in range(len(x_time_list)):
    ax.scatter(lateral_acceleration_list[i], roll_angle_list[i])
    plt.xlabel("Lateral Acceleration [g]")
    plt.ylabel("Roll Angle [deg]")
    plt.title("Speed Vs Lateral Acceleration")
    plt.grid(True)

# Fit data to the slip angles.
#Polinomial Fitting
_, ax = plt.subplots()
for i in range(len(x_time_list)):
    try:
        fitted_data = np.polyfit(lateral_acceleration_list[i], roll_angle_list[i], 2)
        min_lin = np.min(lateral_acceleration_list[i])
        max_lin = np.max(lateral_acceleration_list[i])
        xp = np.linspace(min_lin, max_lin, 100)
        p_1 = np.poly1d(fitted_data)
        ax.scatter(lateral_acceleration_list[i], roll_angle_list[i], c=GlobalTestFile.color_pallete_dark[i], edgecolors=None)
        ax.plot(xp, p_1(xp), c=GlobalTestFile.color_pallete_light[i], linewidth=3.0)
        ax.legend(["Tire A", "Tire B"])
        plt.xlabel("Lateral Acceleration [g]")
        plt.ylabel("Roll Angle [deg]")
        plt.grid(True)
        plt.draw()
        plt.pause(0.1)
    except:
        print("It was not possible to fit the data of test: " + str(GlobalTestFile.TEST_NUMBER))


plt.show()