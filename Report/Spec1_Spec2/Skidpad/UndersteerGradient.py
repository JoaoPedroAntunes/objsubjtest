"""
Joao Antunes
Calculate the understeer grandient for each of the tires
"""
import numpy as np
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal
from scipy.interpolate import CubicSpline

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_tests_spec1 = import_csv("sunday_skidpad_spec1")
df_tests_spec2 = import_csv("monday_skidpad_spec2")

# Select test to be plotted.
selected_data_spec1 = df_tests_spec1[1]
selected_data_spec2 = df_tests_spec2[5]

# Pre-Process some data.



########################################################################################################################
# Define variable
x_time_1 = selected_data_spec1.loc[:, "Time"]
x_time_2 = selected_data_spec2.loc[:, "Time"]
ay_1 = np.abs(selected_data_spec1["ADMA Accel Body Y"].loc[np.abs(selected_data_spec1["ADMA Accel Body Y"]) > 0.1])
ay_2 = np.abs(selected_data_spec2["ADMA Accel Body Y"].loc[np.abs(selected_data_spec2["ADMA Accel Body Y"]) > 0.1])
steering_angle_1 = np.abs(selected_data_spec1["Steered Angle"].loc[np.abs(selected_data_spec1["ADMA Accel Body Y"]) > 0.1])
steering_angle_2 = np.abs(selected_data_spec2["Steered Angle"].loc[np.abs(selected_data_spec2["ADMA Accel Body Y"]) > 0.1])

# Filtered data
f_ay_1 = signal.medfilt(ay_1, 201)
f_ay_2 = signal.medfilt(ay_2, 201)
f_steering_angle_1 = signal.medfilt(steering_angle_1, 11)
f_steering_angle_2 = signal.medfilt(steering_angle_2, 11)
########################################################################################################################
# Understeer Gradient - Raw Data
_, ax = plt.subplots()
ax.scatter(ay_1, np.abs(steering_angle_1), color="darkred")
ax.scatter(ay_2, steering_angle_2, color="darkgreen")
ax.legend(["Tire A", "Tire B"])
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Understeer Gradient")
plt.grid(True)

########################################################################################################################
# Understeer Gradient - Filtered Data
_, ax = plt.subplots()
ax.scatter(f_ay_1, np.abs(f_steering_angle_1), color="red")
ax.scatter(f_ay_2, f_steering_angle_2, color="green")
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Understeer Gradient")
plt.grid(True)

########################################################################################################################
# Plot all the steering wheel angles together
_, ax = plt.subplots()
legend = []
i=0
for test in df_tests_spec1:
    ax.plot(test.loc[:, "Time"], test.loc[:, "Steered Angle"], color= "darkred")
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Time [s]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)

for test in df_tests_spec2:
    ax.plot(test.loc[:, "Time"], test.loc[:, "Steered Angle"], color="darkblue")
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Time [s]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)


########################################################################################################################
# Plot all the steering wheel angles together
_, ax = plt.subplots()
legend = []
i=0
for test in df_tests_spec1:
    ax.plot(test.loc[:, "ADMA Accel Body Y"], test.loc[:, "Steered Angle"])
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)

for test in df_tests_spec2:
    ax.plot(test.loc[:, "ADMA Accel Body Y"], test.loc[:, "Steered Angle"])
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)


########################################################################################################################
# Plot all the steering wheel angles together

# Plot all the steering wheel angles together
_, ax = plt.subplots()
legend = []
i=0
for test in df_tests_spec1:
    ax.plot(signal.medfilt(test.loc[:, "ADMA Accel Body Y"],201), test.loc[:, "Steered Angle"])
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)

for test in df_tests_spec2:
    ax.plot(signal.medfilt(test.loc[:, "ADMA Accel Body Y"],201), test.loc[:, "Steered Angle"],"--")
    legend.append("T" + str(i))
    i = i + 1
ax.legend(legend)
plt.xlabel("Lateral Acceleration [g]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.title("Steering Wheel Angle Versus Time")
plt.grid(True)




########################################################################################################################
# Calculate the steering derivative
x=selected_data_spec1.loc[:, "ADMA Accel Body Y"]
steering_angle_speed = np.gradient(x, 0.002)
filter = steering_angle_speed>=0
swa = selected_data_spec1["Steered Angle"].where(filter)
ay = selected_data_spec1["ADMA Accel Body Y"].where(filter)
f_ay_1 = signal.medfilt(ay, 201)

_, ax = plt.subplots()
#ax.plot(selected_data_spec1.loc[:, "Time"],selected_data_spec1["ADMA Accel Body Y"])
ax.plot(selected_data_spec1.loc[:, "Time"],ay)


########################################################################################################################
# Fit curve to data
_, ax = plt.subplots()
try:
    # Get data for linear fit 0g ->0.4g
    linear_range_ay_1 = f_ay_1[f_ay_1 <= 0.4]
    linear_range_ay_2 = f_ay_2[f_ay_2 <= 0.4]
    linear_range_steering_angle_1 = f_steering_angle_1[f_ay_1 <= 0.4]
    linear_range_steering_angle_2 = f_steering_angle_2[f_ay_2 <= 0.4]
    fit_linear_range_1 = np.polyfit(linear_range_ay_1, linear_range_steering_angle_1, 1)
    p_1 = np.poly1d(fit_linear_range_1)
    min_lin = np.min(linear_range_ay_1)
    max_lin = np.max(linear_range_ay_1)
    xp = np.linspace(min_lin, max_lin, 100)
    ax.scatter(linear_range_ay_1, linear_range_steering_angle_1)
    ax.plot(xp, p_1(xp), linewidth=3.0)


    non_linear_range_ay_1 = f_ay_1[f_ay_1 > 0.4]
    non_linear_range_ay_2 = f_ay_2[f_ay_2 > 0.4]
    non_linear_range_steering_angle_1 = f_steering_angle_1[f_ay_1 > 0.4]
    non_linear_range_steering_angle_2 = f_steering_angle_2[f_ay_2 > 0.4]

    cs = np.polyfit(non_linear_range_ay_1, non_linear_range_steering_angle_1, 2)
    p_1 = np.poly1d(cs)
    min_lin = np.min(non_linear_range_ay_1)
    max_lin = np.max(non_linear_range_ay_1)
    x_range = np.linspace(min_lin, max_lin, 100)
    ax.scatter(non_linear_range_ay_1, non_linear_range_steering_angle_1)
    ax.plot(x_range, p_1(x_range), linewidth=3.0)


except:
    print("It was not possible to fit the data of test: " + str(GlobalTestFile.TEST_NUMBER))

plt.show()