"""
Joao Antunes
Script that show the 12 cases of the yaw moment
"""
from ImportData.ImportTestData import import_csv
import forces
import vehicle
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_tests_spec1 = import_csv("sunday_step_steer_spec1")
df_tests_spec2 = import_csv("monday_step_steer_spec2")

# Select test to be plotted.
selected_data_spec1 = df_tests_spec1[1]
selected_data_spec2 = df_tests_spec2[3]



selected_datas = [selected_data_spec1, selected_data_spec2]
#########################################################
# The following section represents the processing of the forces. What we are trying to achieve here is to calculate
# the forces and moments X,Y,Z and calculate what is the 12 causes of the yaw moment.

# Cropping Data
selected_data_spec1 = selected_data_spec1.loc[selected_data_spec1["Time"] < 1.5]
selected_data_spec2 = selected_data_spec2.loc[selected_data_spec2["Time"] < 1.5]
selected_data_spec2["Time"]  = selected_data_spec2.loc[:, "Time"] + 0.43

# Get the vehicle properties
front_wheelbase = vehicle.get_front_wheelbase()  # a
rear_wheelbase = vehicle.get_rear_wheelbase()  # b
front_track = vehicle.front_track
rear_track = vehicle.rear_track

sum_moments_fx_spec1 = forces.get_sum_moments_fx(selected_data_spec1)
sum_moments_fy_spec1 = forces.get_sum_moments_fy(selected_data_spec1)
sum_moments_mz_spec1 = forces.get_sum_moments_mz(selected_data_spec1)

#Calculate total yaw moment
yaw_moment_spec1 = sum_moments_fx_spec1 + sum_moments_fy_spec1 + sum_moments_mz_spec1

sum_moments_fx_spec2 = forces.get_sum_moments_fx(selected_data_spec2)
sum_moments_fy_spec2 = forces.get_sum_moments_fy(selected_data_spec2)
sum_moments_mz_spec2 = forces.get_sum_moments_mz(selected_data_spec2)

yaw_moment_spec2 = sum_moments_fx_spec2 + sum_moments_fy_spec2 + sum_moments_mz_spec2

# Smoothing
f_sum_moments_fx_spec1 = signal.medfilt(sum_moments_fx_spec1, 21)
f_sum_moments_fy_spec1 = signal.medfilt(sum_moments_fy_spec1, 11)
f_sum_moments_mz_spec1 = signal.medfilt(sum_moments_mz_spec1, 31)

f_sum_moments_fx_spec2 = signal.medfilt(sum_moments_fx_spec2, 21)
f_sum_moments_fy_spec2 = signal.medfilt(sum_moments_fy_spec2, 11)
f_sum_moments_mz_spec2 = signal.medfilt(sum_moments_mz_spec2, 31)

f_yaw_moment_spec1 = signal.medfilt(yaw_moment_spec1, 11)
f_yaw_moment_spec2 = signal.medfilt(yaw_moment_spec2, 11)



# Plotting

# Plot individual contribution of Fx to the yaw moment
_, ax = plt.subplots()
ax.plot(selected_data_spec1.loc[:, "Time"], f_sum_moments_fx_spec1, color="darkred")
ax.plot(selected_data_spec2.loc[:, "Time"], f_sum_moments_fx_spec2, color="darkgreen")
ax.legend(["Tire A", "Tire B"])
plt.title("Yaw Moment Fx")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")

# Plot individual contribution of Fx to the yaw moment
_, ax = plt.subplots()
ax.plot(selected_data_spec1.loc[:, "Time"], f_sum_moments_fy_spec1, color="darkred")
ax.plot(selected_data_spec2.loc[:, "Time"], f_sum_moments_fy_spec2, color="darkgreen")
ax.legend(["Tire A", "Tire B"])
plt.title("Yaw Moment Fy")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")

_, ax = plt.subplots()
ax.plot(selected_data_spec1.loc[:, "Time"], f_sum_moments_mz_spec1, color="darkred")
ax.plot(selected_data_spec2.loc[:, "Time"], f_sum_moments_mz_spec2, color="darkgreen")
ax.legend(["Tire A", "Tire B"])
plt.title("Yaw Moment Mz")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")

_, ax = plt.subplots()
ax.plot(selected_data_spec1.loc[:, "Time"], f_yaw_moment_spec1, color="darkred")
ax.plot(selected_data_spec2.loc[:, "Time"], f_yaw_moment_spec2, color="darkgreen")
ax.legend(["Tire A", "Tire B"])
plt.title("Yaw Moment")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")


plt.show()