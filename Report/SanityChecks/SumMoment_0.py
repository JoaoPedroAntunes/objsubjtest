"""
Joao Antunes
# In steady state the sum of the moments needs to be zero
# M = 0 -> (Fy_FL + Fy_FR) * a - (Fy_RL + Fy_RR) * b = 0
"""
import numpy as np

import vehicle
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

Fy_FL = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_FL"]
Fy_FR = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_FR"]
Fy_RL = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_RL"]
Fy_RR = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_RR"]
a = vehicle.get_front_wheelbase()
b = vehicle.get_rear_wheelbase()

M = (Fy_FL + Fy_FR) * a - (Fy_RL + Fy_RR) * b

fig, ax = plt.subplots()
ax.plot(selected_data.loc[:,"Time"],M)
ax.legend(["Sum of Moments"])
plt.xlabel("Time (s)")
plt.ylabel("Moment (Nm)")

plt.show()

