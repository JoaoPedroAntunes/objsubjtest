"""
Joao Antunes
Check if F = ma
"""
import numpy as np

import forces
import vehicle
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

########################################################################################################################
#Check if the sum of the lateral forces are equal to the mass times the lateral acceleration
fy = forces.sum_forces(selected_data, "Y")
ma = selected_data.loc[:, "ADMA Accel Body Y"] * vehicle.get_total_weight()

fig5, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], fy)
ax.plot(selected_data.loc[:, "Time"], ma)
ax.legend(["Sum of Fy", "Mass*Acc"])
plt.xlabel("Time (s)")
plt.ylabel("Force (N)")
plt.grid(True)

########################################################################################################################
#Check if the sum of the longitudinal forces are equal to the mass time the longitudinal accelearation
fx = forces.sum_forces(selected_data, "X")
ma_x = selected_data.loc[:, "ADMA Accel Body X"] * vehicle.get_total_weight()

_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], fx)
ax.plot(selected_data.loc[:, "Time"], ma_x)
ax.legend(["Sum of Fx", "Mass*Acc"])
plt.xlabel("Time (s)")
plt.ylabel("Force (N)")


plt.show()
