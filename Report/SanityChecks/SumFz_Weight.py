"""
Joao Antunes
The following script calculates the total weight of the car
by summing the Fz. It also calculates the front and rear weight
distribution and the cross weight
"""

# TODO: Calculate the total weight of the car
# Todo Calculate the weight distribution
# TODO: Calculate teh cross weight
import numpy as np

import forces
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

########################################################################################################################
# Define variable
x_time = selected_data.loc[:, "Time"]

slip_angle_FL = np.degrees(selected_data.loc[:, "slip_angle_FL"])
slip_angle_FR = np.degrees(selected_data.loc[:, "slip_angle_FR"])
slip_angle_RL = np.degrees(selected_data.loc[:, "slip_angle_RL"])
slip_angle_RR = np.degrees(selected_data.loc[:, "slip_angle_RR"])

Fy_FL = forces.get_force(selected_data, "Y", "FL")
Fy_FR = forces.get_force(selected_data, "Y", "FR")
Fy_RL = forces.get_force(selected_data, "Y", "RL")
Fy_RR = forces.get_force(selected_data, "Y", "RR")

Fz_FL = forces.get_force(selected_data, "Z", "FL")
Fz_FR = forces.get_force(selected_data, "Z", "FR")
Fz_RL = forces.get_force(selected_data, "Z", "RL")
Fz_RR = forces.get_force(selected_data, "Z", "RR")

########################################################################################################################
# Plot the slip angle versus Fy versus Fz
fig, ax = plt.subplots(2, 2)
b = ax[0, 0].scatter(slip_angle_FL, Fy_FL, c=Fy_FL/Fz_FL, cmap="hsv")
b1 = ax[0, 1].scatter(slip_angle_FR, Fy_FR, c=Fy_FR/Fz_FR, cmap="hsv")
b2 = ax[1, 0].scatter(slip_angle_RL, Fy_RL, c=Fy_RL/Fz_RL, cmap="hsv")
b3 = ax[1, 1].scatter(slip_angle_RR, Fy_RR, c=Fy_RR/Fz_RR, cmap="hsv")
ax[0, 0].legend(["FL"])
ax[0, 1].legend(["FR"])
ax[1, 0].legend(["RL"])
ax[1, 1].legend(["RR"])
ax[0, 0].set_xlabel("Time [s]")
ax[0, 1].set_xlabel("Time [s]")
ax[1, 0].set_xlabel("Time [s]")
ax[1, 1].set_xlabel("Time [s]")
ax[0, 0].set_ylabel("Fy [N]")
ax[1, 0].set_ylabel("Fy [N]")
ax[0, 1].set_ylabel("Fy [N]")
ax[1, 1].set_ylabel("Fy [N]")
# Add a colorbar
fig.colorbar(b, ax = ax[0, 0])
fig.colorbar(b1, ax = ax[0, 1])
fig.colorbar(b2, ax = ax[1, 0])
fig.colorbar(b3, ax = ax[1, 1])


plt.show()