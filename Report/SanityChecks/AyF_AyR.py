"""
Joao Antunes
Compares the front and rear axle acceleration
In steady state the front axle and rear axle acceleration should be the same
A_F = A_R -> (Fy_FL + Fy_FR) / M_F = (Fy_RL + Fy_RR) / M_R
"""
import numpy as np

import vehicle
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal


# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

Fy_FL = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_FL"]
Fy_FR = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_FR"]
Fy_RL = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_RL"]
Fy_RR = selected_data.loc[:, "WFT_Force_Y_Vehicle_Body_RR"]
M_f = vehicle.total_weight_front
M_r = vehicle.total_weight_rear

Ay_F = np.abs((Fy_FL + Fy_FR) / M_f)
Ay_R = np.abs((Fy_RL + Fy_RR) / M_r)

Ay_F = signal.medfilt(Ay_F, 201)
Ay_R = signal.medfilt(Ay_R, 201)

fig, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], Ay_F)
ax.plot(selected_data.loc[:, "Time"], Ay_R)
ax.legend(["Front Axle Acceleration", "Rear Axle Acceleration"])
plt.xlabel("Time [s]")
plt.ylabel("Lateral Acceleration [g]")
plt.grid(True)

fig, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Vehicle Speed"], Ay_F)
ax.plot(selected_data.loc[:, "Vehicle Speed"], Ay_R)
ax.legend(["Front Axle Acceleration", "Rear Axle Acceleration"])
plt.xlabel("Vehicle Speed [Km/h]")
plt.ylabel("Lateral Acceleration [g]")
plt.grid(True)

plt.show()
