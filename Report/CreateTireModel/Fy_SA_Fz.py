"""
Joao Antunes
Plot's the Slip Angles and compares with the forces and temperatures.
"""
import numpy as np
import pandas as pd
import forces
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D


# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
#selected_data = df_tests[GlobalTestFile.TEST_NUMBER]
selected_data = pd.concat([df_tests[GlobalTestFile.TEST_NUMBER]])

# Crop Data
selected_data = selected_data.loc[np.abs(selected_data["ADMA Accel Body Y"]) > 0.2]

########################################################################################################################
# Define variable
x_time = selected_data.loc[:, "Time"]

slip_angle_FL = np.degrees(selected_data.loc[:, "slip_angle_FL"])
slip_angle_FR = np.degrees(selected_data.loc[:, "slip_angle_FR"])
slip_angle_RL = np.degrees(selected_data.loc[:, "slip_angle_RL"])
slip_angle_RR = np.degrees(selected_data.loc[:, "slip_angle_RR"])

Fy_FL = forces.get_force(selected_data, "Y", "FL")
Fy_FR = forces.get_force(selected_data, "Y", "FR")
Fy_RL = forces.get_force(selected_data, "Y", "RL")
Fy_RR = forces.get_force(selected_data, "Y", "RR")

Fz_FL = forces.get_force(selected_data, "Z", "FL")
Fz_FR = forces.get_force(selected_data, "Z", "FR")
Fz_RL = forces.get_force(selected_data, "Z", "RL")
Fz_RR = forces.get_force(selected_data, "Z", "RR")

tire_temp_inner_FL = selected_data.loc[:, "Temp Tyre 1 Inner FL"]
tire_temp_center_FL = selected_data.loc[:, "Temp Tyre 3 Centre FL"]
tire_temp_outer_FL = selected_data.loc[:, "Temp Tyre 5 Outer FL"]
avg_temp_FL = (tire_temp_inner_FL + tire_temp_center_FL + tire_temp_outer_FL)/3
# Convert Data to User Coordinate System
# Fz
Fz_FL = np.abs(Fz_FL)
Fz_FR = np.abs(Fz_FR)
Fz_RL = np.abs(Fz_RL)
Fz_RR = np.abs(Fz_RR)

#Fy
Fy_FL = np.abs(Fy_FL)
Fy_FR = np.abs(Fy_FR)
Fy_RL = np.abs(Fy_RL)
Fy_RR = np.abs(Fy_RR)

# Filter Data
slip_angle_FL = signal.medfilt(slip_angle_FL, 201)
slip_angle_FR = signal.medfilt(slip_angle_FR, 201)
slip_angle_RL = signal.medfilt(slip_angle_RL, 201)
slip_angle_RR = signal.medfilt(slip_angle_RR, 201)

Fy_FL = signal.medfilt(Fy_FL, 201)
Fy_FR = signal.medfilt(Fy_FR, 201)
Fy_RL = signal.medfilt(Fy_RL, 201)
Fy_RR = signal.medfilt(Fy_RR, 201)

Fz_FL = signal.medfilt(Fz_FL, 201)
Fz_FR = signal.medfilt(Fz_FR, 201)
Fz_RL = signal.medfilt(Fz_RL, 201)
Fz_RR = signal.medfilt(Fz_RR, 201)

########################################################################################################################
# This section is responsible for plotting the slip angles and further analysis with Slip Angles
_, ax = plt.subplots()
ax.plot(x_time, slip_angle_FL, color="darkred")
ax.plot(x_time, slip_angle_FR, color="darkgreen")
ax.plot(x_time, slip_angle_RL, color="darkblue")
ax.plot(x_time, slip_angle_RR, color="orange")
ax.legend(["FL", "FR", "RL", "RR"])
plt.xlabel("Time [s]")
plt.ylabel("Angle [deg]")
plt.title("Wheel Slip Angle")
plt.grid(True)

# Fit data to the slip angles.
#Polinomial Fitting
fitted_data_FL = np.polyfit(x_time, slip_angle_FL, 2)
fitted_data_FR = np.polyfit(x_time, slip_angle_FR, 2)
fitted_data_RL = np.polyfit(x_time, slip_angle_RL, 2)
fitted_data_RR = np.polyfit(x_time, slip_angle_RR, 2)
xp = np.linspace(0.1, 30, 100)
p_1 = np.poly1d(fitted_data_FL)
p_2 = np.poly1d(fitted_data_FR)
p_3 = np.poly1d(fitted_data_RL)
p_4 = np.poly1d(fitted_data_RR)
_, ax = plt.subplots(2,2)
ax[0,0].plot(x_time, slip_angle_FL, c="darkred")
ax[0,1].plot(x_time, slip_angle_FR, c="darkgreen")
ax[1,0].plot(x_time, slip_angle_RL, c="darkblue")
ax[1,1].plot(x_time, slip_angle_RR, c="orange")
#ax[0,0].plot(xp, p_1(xp), color="red",linewidth=3.0)
#ax[0,1].plot(xp, p_2(xp), color="green",linewidth=3.0)
#ax[1,0].plot(xp, p_3(xp), color="blue",linewidth=3.0)
#ax[1,1].plot(xp, p_4(xp), color="yellow",linewidth=3.0)
ax[0,0].legend(["FL Slip Angle"])
ax[0,1].legend(["FR Slip Angle"])
ax[1,0].legend(["RL Slip Angle"])
ax[1,1].legend(["RR Slip Angle"])
ax[0,0].set_xlabel("Time [s]")
ax[0,1].set_xlabel("Time [s]")
ax[1,0].set_xlabel("Time [s]")
ax[1,1].set_xlabel("Time [s]")
ax[0,0].set_ylabel("Angle [deg]")
ax[0,1].set_ylabel("Angle [deg]")
ax[1,0].set_ylabel("Angle [deg]")
ax[1,1].set_ylabel("Angle [deg]")
ax[0,0].grid(True)
ax[0,1].grid(True)
ax[1,0].grid(True)
ax[1,1].grid(True)


########################################################################################################################
# Plot the slip angle versus Fy
_, ax = plt.subplots(2, 2)
ax[0, 0].plot(slip_angle_FL, Fy_FL)
ax[0, 1].plot(slip_angle_FR, Fy_FR)
ax[1, 0].plot(slip_angle_RL, Fy_RL)
ax[1, 1].plot(slip_angle_RR, Fy_RR)

########################################################################################################################

# Plot the slip angle versus Fy versus Fz
fig, ax = plt.subplots(2, 2, sharey=True)
min_fz = np.min([Fz_FL,Fz_FR,Fz_RL,Fz_RR])
max_fz = np.max([Fz_FL,Fz_FR,Fz_RL,Fz_RR])
b = ax[0, 0].scatter(slip_angle_FL, Fy_FL, c=Fz_FL, cmap="hsv", vmin=min_fz, vmax=max_fz)
b1 = ax[0, 1].scatter(slip_angle_FR, Fy_FR, c=Fz_FR, cmap="hsv", vmin=min_fz, vmax=max_fz)
b2 = ax[1, 0].scatter(slip_angle_RL, Fy_RL, c=Fz_RL, cmap="hsv", vmin=min_fz, vmax=max_fz)
b3 = ax[1, 1].scatter(slip_angle_RR, Fy_RR, c=Fz_RR, cmap="hsv", vmin=min_fz, vmax=max_fz)
ax[0, 0].legend(["FL"])
ax[0, 1].legend(["FR"])
ax[1, 0].legend(["RL"])
ax[1, 1].legend(["RR"])
ax[0, 0].set_xlabel("Slip Angle [deg]")
ax[0, 1].set_xlabel("Slip Angle [deg]")
ax[1, 0].set_xlabel("Slip Angle [deg]")
ax[1, 1].set_xlabel("Slip Angle [deg]")
ax[0, 0].set_ylabel("Fy [N]")
ax[1, 0].set_ylabel("Fy [N]")
ax[0, 1].set_ylabel("Fy [N]")
ax[1, 1].set_ylabel("Fy [N]")
# Add a colorbar
fig.colorbar(b, ax = ax[0, 0])
fig.colorbar(b1, ax = ax[0, 1])
fig.colorbar(b2, ax = ax[1, 0])
fig.colorbar(b3, ax = ax[1, 1])
ax[0,0].grid(True)
ax[0,1].grid(True)
ax[1,0].grid(True)
ax[1,1].grid(True)

#
# Plot 3D Fy-Fz-SA
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(slip_angle_FL, Fz_FL/1000, Fy_FL/1000, c="darkgreen")
ax.set_xlabel('Slip Angle [deg]')
ax.set_ylabel('Vertical Force [kN]')
ax.set_zlabel('Lateral Force [kN]')

# Plot 3D Fy-Fz-SA - Color
fig = plt.figure()
ax = plt.axes(projection='3d')
b = ax.scatter3D(slip_angle_FL, Fz_FL/1000, Fy_FL/1000, c=avg_temp_FL, cmap=plt.cm.get_cmap('RdBu_r'))
ax.set_xlabel('Slip Angle [deg]')
ax.set_ylabel('Vertical Force [kN]')
ax.set_zlabel('Lateral Force [kN]')
fig.colorbar(b)
plt.show()
