"""
Joao Antunes
Analyzes the drivers inputs
Plots the steering, speed, throttle, ax, ay
"""
import numpy as np
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal


# Define matplotlib style
GlobalTestFile.set_matplotlib_style()


def run_report(df_tests):

    if df_tests is None:
        # Import data
        df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)
    else:
        pass

    # Select test to be plotted.
    selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

    plot_raw_driver_inputs(selected_data)

    plot_filtered_driver_inputs(selected_data)


def plot_raw_driver_inputs(selected_data):
    # The following section corresponds to analysing the drivers inputs. The main goals of this section is to understand
    # if the driver was able to maintain constant inputs during the steps, also look to see if the driver had any
    # problem with things like under/oversteer.
    _, ax = plt.subplots(5, sharex=True)
    ax[0].plot(selected_data.loc[:, "Time"], np.abs(selected_data.loc[:, "Steered Angle"]), color="darkred")
    ax[1].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Vehicle Speed"], color="darkgreen")
    ax[2].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Throttle Pos"], color="purple")
    ax[3].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "ADMA Accel Body X"], color="darkblue")
    ax[4].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "ADMA Accel Body Y"], color="orange")
    ax[0].get_xaxis().set_visible(False)
    ax[1].get_xaxis().set_visible(False)
    ax[2].get_xaxis().set_visible(False)
    ax[0].legend(["Steering angle"], loc=1)
    ax[1].legend(["Speed"], loc=1)
    ax[2].legend(["Throttle"], loc=1)
    ax[3].legend(["Long acc"], loc=1)
    ax[4].legend(["Lat acc"], loc=1)
    ax[0].set_ylabel('Angle [deg]')
    ax[1].set_ylabel('Speed [kph]')
    ax[2].set_ylabel('Position [%]')
    ax[3].set_ylabel('Acceleration [g]')
    ax[4].set_ylabel('Acceleration [g]')
    ax[0].grid(1, linestyle='--')
    ax[1].grid(1, linestyle='--')
    ax[2].grid(1, linestyle='--')
    ax[3].grid(1, linestyle='--')
    ax[4].grid(1, linestyle='--')
    ax[0].set_title("Driver Analysis")
    plt.xlabel("Time [s]")
    plt.draw()
    plt.pause(0.1)

def plot_filtered_driver_inputs(selected_data):
    # The following section is the same as the above it is only responsible for filtering the data
    _, ax = plt.subplots(5, sharex=True)
    ax[0].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Steered Angle"], color="darkred")
    ax[1].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Vehicle Speed"], color="darkgreen")
    ax[2].plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Throttle Pos"], color="purple")
    ax[3].plot(selected_data.loc[:, "Time"], signal.medfilt(selected_data.loc[:, "ADMA Accel Body X"], 201),
               color="darkblue")
    ax[4].plot(selected_data.loc[:, "Time"], signal.medfilt(selected_data.loc[:, "ADMA Accel Body Y"], 201),
               color="orange")
    ax[0].get_xaxis().set_visible(False)
    ax[1].get_xaxis().set_visible(False)
    ax[2].get_xaxis().set_visible(False)
    ax[0].legend(["Steering angle"], loc=1)
    ax[1].legend(["Speed"], loc=1)
    ax[2].legend(["Throttle"], loc=1)
    ax[3].legend(["Long acc"], loc=1)
    ax[4].legend(["Lat acc"], loc=1)
    ax[0].set_ylabel('Angle [deg]')
    ax[1].set_ylabel('Speed [kph]')
    ax[2].set_ylabel('Position [%]')
    ax[3].set_ylabel('Acceleration [g]')
    ax[4].set_ylabel('Acceleration [g]')
    ax[0].grid(1, linestyle='--')
    ax[1].grid(1, linestyle='--')
    ax[2].grid(1, linestyle='--')
    ax[3].grid(1, linestyle='--')
    ax[4].grid(1, linestyle='--')
    ax[0].set_title("Driver Analysis")
    plt.xlabel("Time [s]")
    plt.draw()
    plt.pause(0.1)


if __name__ == "__main__":
    run_report(None)
    plt.show()
