"""
Joao Antunes
Plot's the Slip Angles and compares with the forces and temperatures.
"""
import numpy as np
import pandas as pd
import forces
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

def run_report(df_tests):

    if df_tests is None:
        # Import data
        df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)
    else:
        pass

    # Select test to be plotted.
    selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

    ########################################################################################################################
    # Define variable
    x_time = selected_data.loc[:, "Time"]

    slip_angle_FL = np.degrees(selected_data.loc[:, "slip_angle_FL"])
    slip_angle_FR = np.degrees(selected_data.loc[:, "slip_angle_FR"])
    slip_angle_RL = np.degrees(selected_data.loc[:, "slip_angle_RL"])
    slip_angle_RR = np.degrees(selected_data.loc[:, "slip_angle_RR"])

    ########################################################################################################################
    # This section is responsible for plotting the slip angles and further analysis with Slip Angles
    _, ax = plt.subplots()
    ax.plot(x_time, slip_angle_FL, color="darkred")
    ax.plot(x_time, slip_angle_FR, color="darkgreen")
    ax.plot(x_time, slip_angle_RL, color="darkblue")
    ax.plot(x_time, slip_angle_RR, color="orange")
    ax.legend(["FL", "FR", "RL", "RR"])
    plt.xlabel("Time [s]")
    plt.ylabel("Angle [deg]")
    plt.title("Wheel Slip Angle")
    plt.grid(True)

    # Fit data to the slip angles.
    #Polinomial Fitting
    try:
        fitted_data_FL = np.polyfit(x_time, slip_angle_FL, 2)
        fitted_data_FR = np.polyfit(x_time, slip_angle_FR, 2)
        fitted_data_RL = np.polyfit(x_time, slip_angle_RL, 2)
        fitted_data_RR = np.polyfit(x_time, slip_angle_RR, 2)
        xp = np.linspace(0.1, 30, 100)
        p_1 = np.poly1d(fitted_data_FL)
        p_2 = np.poly1d(fitted_data_FR)
        p_3 = np.poly1d(fitted_data_RL)
        p_4 = np.poly1d(fitted_data_RR)
        _, ax = plt.subplots(2,2)
        ax[0,0].scatter(x_time, slip_angle_FL, c="darkred", edgecolors=None)
        ax[0,1].scatter(x_time, slip_angle_FR, c="darkgreen", edgecolors=None)
        ax[1,0].scatter(x_time, slip_angle_RL, c="darkblue", edgecolors=None)
        ax[1,1].scatter(x_time, slip_angle_RR, c="orange", edgecolors=None)
        ax[0,0].plot(xp, p_1(xp), color="red",linewidth=3.0)
        ax[0,1].plot(xp, p_2(xp), color="green",linewidth=3.0)
        ax[1,0].plot(xp, p_3(xp), color="blue",linewidth=3.0)
        ax[1,1].plot(xp, p_4(xp), color="yellow",linewidth=3.0)
        ax[0,0].legend(["FL Slip Angle"])
        ax[0,1].legend(["FR Slip Angle"])
        ax[1,0].legend(["RL Slip Angle"])
        ax[1,1].legend(["RR Slip Angle"])
        plt.xlabel("Time [s]")
        plt.grid(True)
        plt.draw()
        plt.pause(0.1)
    except:
        print("It was not possible to fit the data of test: " + str(GlobalTestFile.TEST_NUMBER))

if __name__ == "__main__":
    run_report(None)
    plt.show()
