"""
Joao Antunes
Script that show the 12 cases of the yaw moment
"""
from ImportData.ImportTestData import import_csv
import forces
import vehicle
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

########################################################################################################################
# The following section represents the processing of the forces. What we are trying to achieve here is to calculate
# the forces and moments X,Y,Z and calculate what is the 12 causes of the yaw moment.

sum_fx = forces.sum_forces(selected_data, "X")
sum_fy = forces.sum_forces(selected_data, "Y")
sum_fz = forces.sum_forces(selected_data, "Z")
sum_mx = forces.sum_moments(selected_data, "X")
sum_my = forces.sum_moments(selected_data, "Y")
sum_mz = forces.sum_moments(selected_data, "Z")

fig, ax = plt.subplots(6)
ax[0].plot(selected_data.loc[:, "Time"], sum_fx)
ax[1].plot(selected_data.loc[:, "Time"], sum_fy)
ax[2].plot(selected_data.loc[:, "Time"], sum_fz)
ax[3].plot(selected_data.loc[:, "Time"], sum_mx)
ax[4].plot(selected_data.loc[:, "Time"], sum_my)
ax[5].plot(selected_data.loc[:, "Time"], sum_mz)
ax[0].legend(["Sum of Fx"])
ax[1].legend(["Sum of Fy"])
ax[2].legend(["Sum of Fz"])
ax[3].legend(["Sum of Mx"])
ax[4].legend(["Sum of My"])
ax[5].legend(["Sum of Mz"])

########################################################################################################################
# Calculate the 12 causes of the yaw moment. This is achieve by multiplying the forces Fx by the track and Fy by the
# wheelbase and sum the Mz.

# Get the vehicle properties
front_wheelbase = vehicle.get_front_wheelbase()  # a
rear_wheelbase = vehicle.get_rear_wheelbase()  # b
front_track = vehicle.front_track
rear_track = vehicle.rear_track

# Operations
# Yaw moment contribution from Fx
sum_moments_Fx_FL = forces.get_force(selected_data, "X", "FL")
sum_moments_Fx_FR = forces.get_force(selected_data, "X", "FR")
sum_moments_Fx_RL = forces.get_force(selected_data, "X", "RL")
sum_moments_Fx_RR = forces.get_force(selected_data, "X", "RR")
sum_moments_Fx = (sum_moments_Fx_FL * front_track/2) - (sum_moments_Fx_FR * front_track/2) + (sum_moments_Fx_RL * rear_track/2) - (sum_moments_Fx_RR * rear_track/2)

# Yaw moment contribution from Fy
sum_moments_Fy_FL = forces.get_force(selected_data, "Y", "FL")
sum_moments_Fy_FR = forces.get_force(selected_data, "Y", "FR")
sum_moments_Fy_RL = forces.get_force(selected_data, "Y", "RL")
sum_moments_Fy_RR = forces.get_force(selected_data, "Y", "RR")
sum_moments_Fy = (sum_moments_Fy_FR * front_wheelbase) + (sum_moments_Fy_FL * front_wheelbase) - (sum_moments_Fy_RL * rear_wheelbase) - (sum_moments_Fy_RR * rear_wheelbase)

# Yaw moment contribution from Mz
sum_moments_Mz_FL = forces.get_moment(selected_data, "Z", "FL")
sum_moments_Mz_FR = forces.get_moment(selected_data, "Z", "FR")
sum_moments_Mz_RL = forces.get_moment(selected_data, "Z", "RL")
sum_moments_Mz_RR = forces.get_moment(selected_data, "Z", "RR")
sum_moments_Mz = -(sum_moments_Mz_FL + sum_moments_Mz_FR + sum_moments_Mz_RL + sum_moments_Mz_RR)

#Calculate total yaw moment
yaw_moment = sum_moments_Fx + sum_moments_Fy + sum_moments_Mz

# Plotting

# Plot individual contribution of Fx to the yaw moment
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], sum_moments_Fx_FL, color="darkred")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Fx_FR, color="darkgreen")
ax.plot(selected_data.loc[:, "Time"], sum_moments_Fx_RL, color="darkblue")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Fx_RR, color="orange")
ax.legend(["Yaw_Fx_FL", "Yaw_Fx_FR", "Yaw_Fx_RL", "Yaw_Fx_RR"])
plt.title("Yaw Moment from Fx")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")
bot, top = plt.ylim()
plt.ylim(-max(abs(bot), top), max(abs(bot), top))

# Plot the individual contribution of Fy to the yaw moment
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], sum_moments_Fy_FL, color="darkred")
ax.plot(selected_data.loc[:, "Time"], sum_moments_Fy_FR, color="darkgreen")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Fy_RL, color="darkblue")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Fy_RR, color="orange")
ax.legend(["Yaw_Fy_FL", "Yaw_Fy_FR", "Yaw_Fy_RL", "Yaw_Fy_RR"])
plt.title("Yaw Moment from Fy")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")
bot, top = plt.ylim()
plt.ylim(-max(abs(bot), top), max(abs(bot), top))

# Plot the individual contribution of Mz to the yaw moment
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Mz_FL, color="darkred")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Mz_FR, color="darkgreen")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Mz_RL, color="darkblue")
ax.plot(selected_data.loc[:, "Time"], -sum_moments_Mz_RR, color="orange")
ax.legend(["Yaw_Mz_FL", "Yaw_Mz_FR", "Yaw_Mz_RL", "Yaw_Mz_RR"])
plt.title("Yaw Moment from Mz")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")
bot, top = plt.ylim()
plt.ylim(-max(abs(bot), top), max(abs(bot), top))

# Plot the individual contribution of Fx, Fy, Mz in the same chart
_, ax = plt.subplots(3, sharex=True, sharey=True, gridspec_kw={'hspace': 0})
ax[0].plot(selected_data.loc[:, "Time"], sum_moments_Fx_FL/1000, color="darkred")
ax[0].plot(selected_data.loc[:, "Time"], -sum_moments_Fx_FR/1000, color="darkgreen")
ax[0].plot(selected_data.loc[:, "Time"], sum_moments_Fx_RL/1000, color="darkblue")
ax[0].plot(selected_data.loc[:, "Time"], -sum_moments_Fx_RR/1000, color="orange")
ax[1].plot(selected_data.loc[:, "Time"], sum_moments_Fy_FL/1000, color="darkred")
ax[1].plot(selected_data.loc[:, "Time"], sum_moments_Fy_FR/1000, color="darkgreen")
ax[1].plot(selected_data.loc[:, "Time"], -sum_moments_Fy_RL/1000, color="darkblue")
ax[1].plot(selected_data.loc[:, "Time"], -sum_moments_Fy_RR/1000, color="orange")
ax[2].plot(selected_data.loc[:, "Time"], -sum_moments_Mz_FL/1000, color="darkred")
ax[2].plot(selected_data.loc[:, "Time"], -sum_moments_Mz_FR/1000, color="darkgreen")
ax[2].plot(selected_data.loc[:, "Time"], -sum_moments_Mz_RL/1000, color="darkblue")
ax[2].plot(selected_data.loc[:, "Time"], -sum_moments_Mz_RR/1000, color="orange")
ax[0].legend(["Yaw_Fx_FL", "Yaw_Fx_FR", "Yaw_Fx_RL", "Yaw_Fx_RR"], loc=1)
ax[1].legend(["Yaw_Fy_FL", "Yaw_Fy_FR", "Yaw_Fy_RL", "Yaw_Fy_RR"], loc=1)
ax[2].legend(["Yaw_Mz_FL", "Yaw_Mz_FR", "Yaw_Mz_RL", "Yaw_Mz_RR"], loc=1)
ax[0].set_ylabel('Yaw Moment Fx [kNm]')
ax[1].set_ylabel('Yaw Moment Fy [kNm]')
ax[2].set_ylabel('Yaw Moment Mz [kNm]')
ax[0].grid(1, linestyle='--')
ax[1].grid(1, linestyle='--')
ax[2].grid(1, linestyle='--')
ax[0].set_title("Yaw Moment")
plt.xlabel("Time [s]")
bot, top = plt.ylim()
plt.ylim(-max(abs(bot), top), max(abs(bot), top))

# Plot the total Yaw moment available
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], yaw_moment, color="darkred")
plt.title("Total Yaw Moment [Nm]")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Moment [Nm]")
bot, top = plt.ylim()


plt.show()
