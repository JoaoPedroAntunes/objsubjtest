"""
Joao Antunes
Script to process the data for the S-Motion Report
"""
from ImportData.ImportTestData import import_csv
import forces
import vehicle
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_test = import_csv("thursday_SA_comparison_skidpad")

# Select test to be plotted.
selected_data = df_test[0]

# Plot Data
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Steered Angle"], color="darkred")
plt.title("Raw Data")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Steering Angle [deg]")

#########################################################
# Cropping Data
selected_data = selected_data.loc[selected_data["Time"] >= 2.5]

# Get the vehicle properties
front_wheelbase = vehicle.get_front_wheelbase()  # a
rear_wheelbase = vehicle.get_rear_wheelbase()  # b
front_track = vehicle.front_track
rear_track = vehicle.rear_track

time = selected_data.loc[:, "Time"]
SAS1_vx = selected_data.loc[:, "SAS1 Velocity X"]
SAS1_vy = selected_data.loc[:, "SAS1 Velocity Y"]
SAS1_angle = selected_data.loc[:, "SAS1 Angle"]
SAS2_vx = selected_data.loc[:, "SAS2 Velocity X"]
SAS2_vy = selected_data.loc[:, "SAS2 Velocity Y"]
SAS2_angle = selected_data.loc[:, "SAS2 Angle"]
ay = selected_data.loc[:, "ADMA Accel Body Y"]

# Get the Corrected Values:
yaw_rate_corrected = selected_data.loc[:, "Yaw Rate Corrected"]
vy_front_s_motion =  SAS2_vy - (yaw_rate_corrected * (3.14/180)) * (2.135)
vy_rear_s_motion =  SAS1_vy + (yaw_rate_corrected * (3.14/180)) * (2.76)


_, ax = plt.subplots()
ax.plot(time, vy_rear_s_motion, color="darkred")
ax.plot(time, vy_front_s_motion, color="darkgreen")

ax.set_title("Raw Data")
ax.legend(["SAS1", "SAS2"])
ax.grid(True)
ax.set_ylabel("Lat Vel [m/s]")
plt.xlabel("Time [s]")

# Smoothing
f_vy_front_s_motion = signal.medfilt(vy_front_s_motion, 51)

f_vy_rear_s_motion = signal.medfilt(vy_rear_s_motion, 51)

f_ay = signal.medfilt(ay, 501)

_, ax = plt.subplots()
ax.plot(time, f_vy_rear_s_motion, color="darkred")
ax.plot(time, f_vy_front_s_motion, color="darkgreen")

ax.legend(["SAS1", "SAS2"])
ax.grid(True)
ax.set_ylabel("Lateral Velocity [m/s]")
plt.xlabel("Time [s]")

# Repeatibility of the test
# Plot Ay - Side Slip Angle
_, ax = plt.subplots()
ax.scatter(np.abs(signal.decimate(f_ay,5)), signal.decimate(SAS1_angle, 5), color="darkred")
ax.scatter(f_ay, SAS2_angle, color="darkgreen")

plt.xlabel("Time [s]")


plt.show()