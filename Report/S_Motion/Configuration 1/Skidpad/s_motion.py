"""
Joao Antunes
Script to process the data for the S-Motion Report
"""
from ImportData.ImportTestData import import_csv
import forces
import vehicle
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_test = import_csv("wednesday_afternoon_chirp_high")

# Select test to be plotted.
selected_data = df_test[0]

# Plot Data
_, ax = plt.subplots()
ax.plot(selected_data.loc[:, "Time"], selected_data.loc[:, "Steered Angle"], color="darkred")
plt.title("Raw Data")
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Steering Angle [deg]")

#########################################################
# Cropping Data
selected_data = selected_data.loc[selected_data["Time"] >= 2.5]

# Get the vehicle properties
front_wheelbase = vehicle.get_front_wheelbase()  # a
rear_wheelbase = vehicle.get_rear_wheelbase()  # b
front_track = vehicle.front_track
rear_track = vehicle.rear_track

time = selected_data.loc[:, "Time"]
SAS1_vx = selected_data.loc[:, "SAS1 Velocity X"]
SAS1_vy = selected_data.loc[:, "SAS1 Velocity Y"]
SAS1_angle = selected_data.loc[:, "SAS1 Angle"]
SAS2_vx = selected_data.loc[:, "SAS2 Velocity X"]
SAS2_vy = selected_data.loc[:, "SAS2 Velocity Y"]
SAS2_angle = selected_data.loc[:, "SAS2 Angle"]
SAS3_vx = selected_data.loc[:, "SAS3 S350 Velocity X"]
SAS3_vy = selected_data.loc[:, "SAS3 S350 Velocity Y"]
SAS3_angle = selected_data.loc[:, "SAS3 S350 Angle"]
ay = selected_data.loc[:, "ADMA Accel Body Y"]

# Get the Corrected Values:
yaw_rate_corrected = selected_data.loc[:, "Yaw Rate Corrected"]
#vy_front_s_motion =  SAS1_vy - (yaw_rate_corrected * (3.14/180)) * (SAS1_vx * 2.135)
#vy_rear_s_motion =  SAS2_vy + (yaw_rate_corrected * (3.14/180)) * (SAS2_vx * (2.76-0.15))
vy_front_s_motion =  SAS1_vy - (yaw_rate_corrected * (3.14/180)) * (SAS1_vx )
vy_rear_s_motion =  SAS2_vy + (yaw_rate_corrected * (3.14/180)) * (SAS2_vx )

# Plot Raw Data:
"""
_, ax = plt.subplots(3, sharex=True)
ax[0].plot(time, SAS1_vx, color="darkred")
ax[0].plot(time, SAS2_vx, color="darkgreen")
ax[0].plot(time, SAS3_vx, color="darkblue")
ax[1].plot(time, SAS1_vy, color="darkred")
ax[1].plot(time, SAS2_vy+2.5, color="darkgreen")
ax[1].plot(time, SAS3_vy, color="darkblue")
ax[2].plot(time, SAS1_angle, color="darkred")
ax[2].plot(time, SAS2_angle, color="darkgreen")
ax[2].plot(time, SAS3_angle, color="darkblue")

ax[0].set_title("Raw Data")
ax[0].legend(["SAS1", "SAS2", "SAS3 S350"])
ax[1].legend(["SAS1", "SAS2", "SAS3 S350"])
ax[2].legend(["SAS1", "SAS2", "SAS3 S350"])
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)
ax[0].set_ylabel("Long Vel [m/s]")
ax[1].set_ylabel("Lat Vel [m/s]")
ax[2].set_ylabel("Angle [deg]")
plt.xlabel("Time [s]")
"""



_, ax = plt.subplots(3, sharex=True)
ax[0].plot(time, SAS1_vx, color="darkred")
ax[0].plot(time, SAS2_vx, color="darkgreen")
ax[1].plot(time, SAS1_vy, color="darkred")
ax[1].plot(time, SAS2_vy, color="darkgreen")
ax[2].plot(time, SAS1_angle, color="darkred")
ax[2].plot(time, SAS2_angle, color="darkgreen")

ax[0].set_title("Raw Data")
ax[0].legend(["SAS1", "SAS2"])
ax[1].legend(["SAS1", "SAS2"])
ax[2].legend(["SAS1", "SAS2"])
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)
ax[0].set_ylabel("Long Vel [m/s]")
ax[1].set_ylabel("Lat Vel [m/s]")
ax[2].set_ylabel("Angle [deg]")
plt.xlabel("Time [s]")

_, ax = plt.subplots(3, sharex=True)
ax[0].plot(time, SAS1_vx, color="darkred")
ax[0].plot(time, SAS2_vx, color="darkgreen")
ax[1].plot(time, SAS1_vy, color="darkred")
ax[1].plot(time, SAS2_vy, color="darkgreen")
ax[2].plot(time, SAS1_angle, color="darkred")
ax[2].plot(time, SAS2_angle, color="darkgreen")

ax[0].set_title("Raw Data")
ax[0].legend(["SAS1", "SAS2"])
ax[1].legend(["SAS1", "SAS2"])
ax[2].legend(["SAS1", "SAS2"])
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)
ax[0].set_ylabel("Long Vel [m/s]")
ax[1].set_ylabel("Lat Vel [m/s]")
ax[2].set_ylabel("Angle [deg]")
plt.xlabel("Time [s]")

# Smoothing
f_SAS1_vx = signal.medfilt(SAS1_vx, 51)
f_SAS1_vy = signal.medfilt(SAS1_vy, 51)
f_SAS1_angle = signal.medfilt(SAS1_angle, 51)

f_SAS2_vx = signal.medfilt(SAS2_vx, 51)
f_SAS2_vy = signal.medfilt(SAS2_vy, 51)
f_SAS2_angle = signal.medfilt(SAS2_angle, 51)

f_ay = signal.medfilt(ay, 501)

_, ax = plt.subplots(3, sharex=True)
ax[0].plot(time, f_SAS1_vx, color="darkred")
ax[0].plot(time, f_SAS2_vx, color="darkgreen")
ax[1].plot(time, f_SAS1_vy, color="darkred")
ax[1].plot(time, f_SAS2_vy, color="darkgreen")
ax[2].plot(time, f_SAS1_angle, color="darkred")
ax[2].plot(time, f_SAS2_angle, color="darkgreen")


ax[0].legend(["SAS1", "SAS2"])
ax[1].legend(["SAS1", "SAS2"])
ax[2].legend(["SAS1", "SAS2"])
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)
ax[0].set_ylabel("Long Vel [m/s]")
ax[1].set_ylabel("Lat Vel [m/s]")
ax[2].set_ylabel("Angle [deg]")
plt.xlabel("Time [s]")

# Repeatibility of the test
# Plot Ay - Side Slip Angle
_, ax = plt.subplots()
ax.scatter(np.abs(signal.decimate(f_ay,5)), signal.decimate(SAS1_angle, 5), color="darkred")
ax.scatter(f_ay, SAS2_angle, color="darkgreen")

plt.xlabel("Time [s]")


plt.show()