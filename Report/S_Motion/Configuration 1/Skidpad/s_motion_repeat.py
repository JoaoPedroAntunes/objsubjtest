"""
Joao Antunes
Script to process the data for the S-Motion Report
"""
from ImportData.ImportTestData import import_csv
import forces
import vehicle
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_test = import_csv("wednesday_morning_skidpad")

# Select test to be plotted.
#selected_data = [df_test[0], df_test[2], df_test[4], df_test[6]]
#selected_data = [df_test[0], df_test[1], df_test[2], df_test[3], df_test[4], df_test[5], df_test[6],df_test[7]]
selected_data = [df_test[1], df_test[3], df_test[5]]

#########################################################
# Cropping Data
for i in range(len(selected_data)):
    selected_data[i] = selected_data[i].loc[selected_data[i]["Time"] >= 2.5]
    selected_data[i] = selected_data[i].loc[np.abs(selected_data[i]["ADMA Accel Body Y"]) >= 0.2]
    selected_data[i] = selected_data[i].loc[np.abs(selected_data[i]["ADMA Accel Body Y"]) <= 0.8]

# Get the vehicle properties
front_wheelbase = vehicle.get_front_wheelbase()  # a
rear_wheelbase = vehicle.get_rear_wheelbase()  # b
front_track = vehicle.front_track
rear_track = vehicle.rear_track

time = []
SAS1_vx = []
SAS1_vy = []
SAS1_angle = []
SAS2_vx = []
SAS2_vy = []
SAS2_angle = []
SAS3_vx = []
SAS3_vy = []
SAS3_angle = []
ay = []

for data in selected_data:
    time.append(data.loc[:, "Time"])
    SAS1_vx.append(data.loc[:, "SAS1 Velocity X"])
    SAS1_vy.append(data.loc[:, "SAS1 Velocity Y"])
    SAS1_angle.append(data.loc[:, "SAS1 Angle"])
    SAS2_vx.append(data.loc[:, "SAS2 Velocity X"])
    SAS2_vy.append(data.loc[:, "SAS2 Velocity Y"])
    SAS2_angle.append(data.loc[:, "SAS2 Angle"])
    SAS3_vx.append(data.loc[:, "SAS3 S350 Velocity X"])
    SAS3_vy.append(data.loc[:, "SAS3 S350 Velocity Y"])
    SAS3_angle.append(data.loc[:, "SAS3 S350 Angle"])
    ay.append(data.loc[:, "ADMA Accel Body Y"])

f_SAS1_vx = []
f_SAS1_vy = []
f_SAS1_angle = []
f_SAS2_vx = []
f_SAS2_vy = []
f_SAS2_angle = []
f_ay = []

for i in range(len(selected_data)):
    # Smoothing
    f_SAS1_vx.append(signal.medfilt(SAS1_vx[i], 51))
    f_SAS1_vy.append(signal.medfilt(SAS1_vy[i], 51))
    f_SAS1_angle.append(signal.medfilt(SAS1_angle[i], 51))

    f_SAS2_vx.append(signal.medfilt(SAS2_vx[i], 51))
    f_SAS2_vy.append(signal.medfilt(SAS2_vy[i], 51))
    f_SAS2_angle.append(signal.medfilt(SAS2_angle[i], 51))

    f_ay.append(signal.medfilt(ay[i], 501))

# Repeatibility of the test
# Plot Ay - Side Slip Angle
_, ax = plt.subplots()
i =0
legend = []
for ay, SAS1 in zip(f_ay, SAS1_angle):
    sas = np.abs(signal.decimate(SAS1, 10))
    ax.scatter(np.abs(signal.decimate(ay,10)), sas/np.max(sas), alpha = 0.8, edgecolors='none', s = 10)
    legend.append("T" + str(i))
    i = i+1
ax.legend(legend)

########################################################################################################################
# Fit curve to data
_, ax = plt.subplots()
for ay, SAS1 in zip(f_ay, SAS1_angle):
    try:
        # Get data for linear fit 0g ->0.4g
        sas = np.abs(signal.decimate(SAS1, 10))
        fit_linear_range_1 = np.polyfit(np.abs(signal.decimate(ay,10)), sas/np.max(sas)-1, 2)
        p_1 = np.poly1d(fit_linear_range_1)
        min_lin = np.min(ay)
        max_lin = np.max(ay)
        xp = np.linspace(min_lin, max_lin, 100)
        ax.scatter(np.abs(signal.decimate(ay,10)), sas/np.max(sas)-1, alpha = 0.6, s = 10)
        ax.plot(xp, p_1(xp), linewidth=3.0)





    except:
        print("It was not possible to fit the data of test: " + str(GlobalTestFile.TEST_NUMBER))

ax.grid(True)
ax.set_ylabel("Side Slip Angle [deg]")
ax.set_xlabel("Lateral Acceleration [g]")

plt.show()