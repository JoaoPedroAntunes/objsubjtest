import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Constant Radius\\Argentina 2018 Wednesday Demo On-track Morning.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [13.78,
                        67.81,
                        120.20,
                        177.87,
                        229.23,
                        287.77,
                        341.16,
                        386.10]

time_crop_left_list = [49.44,
                       104.30,
                       161.74,
                       215.05,
                       269.97,
                       325.78,
                       370.06,
                       415.75]

#Create the threshold data
left_1 = (df_raw_constant_radius['Time'] > 13) & (df_raw_constant_radius['Time'] <= 49)
right_1 = (df_raw_constant_radius['Time'] > 67) & (df_raw_constant_radius['Time'] <= 104)
left_2 = (df_raw_constant_radius['Time'] > 120) & (df_raw_constant_radius['Time'] <= 161)
right_2 = (df_raw_constant_radius['Time'] > 177) & (df_raw_constant_radius['Time'] <= 215)
left_3 = (df_raw_constant_radius['Time'] > 229) & (df_raw_constant_radius['Time'] <= 269)
right_3 = (df_raw_constant_radius['Time'] > 287) & (df_raw_constant_radius['Time'] <= 325)
left_4 = (df_raw_constant_radius['Time'] > 341) & (df_raw_constant_radius['Time'] <= 370)
right_4 = (df_raw_constant_radius['Time'] > 386) & (df_raw_constant_radius['Time'] <= 415)



#Get the data frame data with the treshold
#Note here the data was recorded the other way around
#Left
df_left_1 = df_raw_constant_radius[right_1]
df_left_2 = df_raw_constant_radius[right_2]
df_left_3 = df_raw_constant_radius[right_3]
df_left_4 = df_raw_constant_radius[right_4]

#Right
df_right_1 = df_raw_constant_radius[left_1]
df_right_2 = df_raw_constant_radius[left_2]
df_right_3 = df_raw_constant_radius[left_3]
df_right_4 = df_raw_constant_radius[left_4]



fig, axs = plt.subplots(5,2)
axs[0,0].plot(df_left_1.loc[:,"Time"], df_left_1.loc[:,"Steered Angle"])
axs[1,0].plot(df_left_2.loc[:,"Time"], df_left_2.loc[:,"Steered Angle"])
axs[2,0].plot(df_left_3.loc[:,"Time"], df_left_3.loc[:,"Steered Angle"])
axs[3,0].plot(df_left_4.loc[:,"Time"], df_left_4.loc[:,"Steered Angle"])


axs[0,1].plot(df_right_1.loc[:,"Time"], df_right_1.loc[:,"Steered Angle"])
axs[1,1].plot(df_right_2.loc[:,"Time"], df_right_2.loc[:,"Steered Angle"])
axs[2,1].plot(df_right_3.loc[:,"Time"], df_right_3.loc[:,"Steered Angle"])
axs[3,1].plot(df_right_4.loc[:,"Time"], df_right_4.loc[:,"Steered Angle"])


axs[0, 0].legend(['Left Turn 1'])
axs[1, 0].legend(['Left Turn 2'])
axs[2, 0].legend(['Left Turn 3'])
axs[3, 0].legend(['Left Turn 4'])


axs[0, 1].legend(['Right Turn 1'])
axs[1, 1].legend(['Right Turn 2'])
axs[2, 1].legend(['Right Turn 3'])
axs[3, 1].legend(['Right Turn 4'])



## Plot all the left and right turns together
fig3, ax3 = plt.subplots(1,2)
ax3[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"Steered Angle"])
ax3[0].plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], df_left_2.loc[:,"Steered Angle"])
ax3[0].plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], df_left_3.loc[:,"Steered Angle"])
ax3[0].plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], df_left_4.loc[:,"Steered Angle"])


ax3[1].plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"])
ax3[1].plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"])
ax3[1].plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"])
ax3[1].plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"])


ax3[0].set(xlabel='time (s)', ylabel='steering angle (deg)', title="Left Turn")
ax3[0].grid()
ax3[1].set(xlabel='time (s)', ylabel='steering angle (deg)', title = "Right Turn")
ax3[1].grid()

ax3[0].legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4"])
ax3[1].legend(['Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4"])



#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], np.abs(df_left_1.loc[:,"Steered Angle"]))
ax5.plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], np.abs(df_left_2.loc[:,"Steered Angle"]))
ax5.plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], np.abs(df_left_3.loc[:,"Steered Angle"]))
ax5.plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], np.abs(df_left_4.loc[:,"Steered Angle"]))


ax5.plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"],'-.')


ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Constant Radius Test")
ax5.legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4",'Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4"])
ax5.grid()
