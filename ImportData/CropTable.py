"""
Joao Antunes
Script file that contains a table with the values to separate the data.
"""
import pandas as pd

def separate_data(crop_points: list, data: pd.DataFrame) -> list:
    """
    Separates the data into the respective tests.
    :param crop_points: list with the time stamps to separate the data
    :param data: DataFrame with the data
    :return: list of test. Each list is a pd.DataFrame
    """
    test_data = []
    for pnt in crop_points:
        split_data = (data['Time'] > pnt[0]) & (data['Time'] <= pnt[1])
        test_data.append(data[split_data])

    return test_data
