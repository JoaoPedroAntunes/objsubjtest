"""
Joao Antunes
Pos process the data
"""
import numpy as np
import math
import  vehicle

def pos_process_WFT(data_frame_total):
    print("Converting WFT to Vehicle Coordinate System")
    for data_frame in data_frame_total:

        # Wheel Force Transducer conversion
        WFT_Fx_SIGN = -1
        WFT_Fy_SIGN = -1
        WFT_Fz_SIGN = 1
        WFT_Mx_SIGN = -1
        WFT_My_SIGN = -1
        WFT_Mz_SIGN = 1

        WFT_Force_X_Tyre_Corrected_FL = (data_frame.loc[:, 'WFT Force X Wheel FL'] * np.cos(((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677)) * math.pi / 180) - data_frame.loc[:, 'WFT Force Z Wheel FL'] * np.sin(((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Fx_SIGN

        WFT_Force_Y_Tyre_Corrected_FL = data_frame.loc[:, 'WFT Force Y Wheel FL'] * WFT_Fy_SIGN

        WFT_Force_Z_Tyre_Corrected_FL = (
            data_frame.loc[:, 'WFT Force X Wheel FL'] * np.sin((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            + data_frame.loc[:, 'WFT Force Z Wheel FL'] * np.cos(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Fz_SIGN


        data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_FL"] = WFT_Force_X_Tyre_Corrected_FL
        data_frame.loc[:, "WFT_Force_Y_Tyre_Corrected_FL"] = WFT_Force_Y_Tyre_Corrected_FL
        data_frame.loc[:, "WFT_Force_Z_Tyre_Corrected_FL"] = WFT_Force_Z_Tyre_Corrected_FL


        # FR
        WFT_Force_X_Tyre_Corrected_FR = (
            data_frame.loc[:, 'WFT Force X Wheel FR'] * np.cos(
                (-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            - data_frame.loc[:, 'WFT Force Z Wheel FR'] * np.sin(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Fx_SIGN

        WFT_Force_Y_Tyre_Corrected_FR = data_frame.loc[:, 'WFT Force Y Wheel FR'] * WFT_Fy_SIGN

        WFT_Force_Z_Tyre_Corrected_FR = (
            data_frame.loc[:, 'WFT Force X Wheel FR'] * np.sin(
                (-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            + data_frame.loc[:, 'WFT Force Z Wheel FR'] * np.cos(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Fz_SIGN

        data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_FR"] = WFT_Force_X_Tyre_Corrected_FR
        data_frame.loc[:, "WFT_Force_Y_Tyre_Corrected_FR"] = WFT_Force_Y_Tyre_Corrected_FR
        data_frame.loc[:, "WFT_Force_Z_Tyre_Corrected_FR"] = WFT_Force_Z_Tyre_Corrected_FR

        # RL
        data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Force X Wheel RL'] * WFT_Fx_SIGN
        data_frame.loc[:, "WFT_Force_Y_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Force Y Wheel RL'] * WFT_Fy_SIGN
        data_frame.loc[:, "WFT_Force_Z_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Force Z Wheel RL'] * WFT_Fz_SIGN

        # RR
        data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Force X Wheel RR'] * WFT_Fx_SIGN
        data_frame.loc[:, "WFT_Force_Y_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Force Y Wheel RR'] * WFT_Fy_SIGN
        data_frame.loc[:, "WFT_Force_Z_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Force Z Wheel RR'] * WFT_Fz_SIGN



        # Moments

        # Moment - FL
        WFT_Moment_X_Tyre_Corrected_FL = (data_frame.loc[:, 'WFT Moment X Wheel FL'] *
                                         np.cos((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
                                         - data_frame.loc[:, 'WFT Moment Z Wheel FL'] *
                                         np.sin(((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) \
                                         * WFT_Mx_SIGN

        WFT_Moment_Y_Tyre_Corrected_FL = data_frame.loc[:, 'WFT Moment Y Wheel FL'] * WFT_My_SIGN

        WFT_Moment_Z_Tyre_Corrected_FL = (
            data_frame.loc[:, 'WFT Moment X Wheel FL'] * np.sin(
                (-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            + data_frame.loc[:, 'WFT Moment Z Wheel FL'] * np.cos(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Mz_SIGN

        data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_FL"] = WFT_Moment_X_Tyre_Corrected_FL
        data_frame.loc[:, "WFT_Moment_Y_Tyre_Corrected_FL"] = WFT_Moment_Y_Tyre_Corrected_FL
        data_frame.loc[:, "WFT_Moment_Z_Tyre_Corrected_FL"] = WFT_Moment_Z_Tyre_Corrected_FL

        # Moment - FR
        WFT_Moment_X_Tyre_Corrected_FR = (
            data_frame.loc[:, 'WFT Moment X Wheel FR'] * np.cos(
                (-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            - data_frame.loc[:, 'WFT Moment Z Wheel FR'] * np.sin(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Mx_SIGN

        WFT_Moment_Y_Tyre_Corrected_FR = data_frame.loc[:, 'WFT Moment Y Wheel FR'] * WFT_My_SIGN

        WFT_Moment_Z_Tyre_Corrected_FR = (data_frame.loc[:, 'WFT Moment X Wheel FR'] * np.sin(
                (-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180)
            + data_frame.loc[:, 'WFT Moment Z Wheel FR'] * np.cos(
                ((-0.0367 * data_frame.loc[:, 'Steered Angle'] + 2.2677) * math.pi / 180))) * WFT_Mz_SIGN

        data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_FR"] = WFT_Moment_X_Tyre_Corrected_FR
        data_frame.loc[:, "WFT_Moment_Y_Tyre_Corrected_FR"] = WFT_Moment_Y_Tyre_Corrected_FR
        data_frame.loc[:, "WFT_Moment_Z_Tyre_Corrected_FR"] = WFT_Moment_Z_Tyre_Corrected_FR

        # Moment - RL
        data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Moment X Wheel RL'] * WFT_Mx_SIGN
        data_frame.loc[:, "WFT_Moment_Y_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Moment Y Wheel RL'] * WFT_My_SIGN
        data_frame.loc[:, "WFT_Moment_Z_Tyre_Corrected_RL"] = data_frame.loc[:, 'WFT Moment Z Wheel RL'] * WFT_Mz_SIGN

        #Moment - RR
        data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Moment X Wheel RR'] * WFT_Mx_SIGN
        data_frame.loc[:, "WFT_Moment_Y_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Moment Y Wheel RR'] * WFT_My_SIGN
        data_frame.loc[:, "WFT_Moment_Z_Tyre_Corrected_RR"] = data_frame.loc[:, 'WFT Moment Z Wheel RR'] * WFT_Mz_SIGN

        convert_to_vehicle_body(data_frame)


def convert_to_vehicle_body(data_frame):
    corners = ["FL","FR","RL","RR"]

    steer_at_wheel = (data_frame.loc[:, "Steered Angle"] * (math.pi / 180)) / vehicle.steering_ratio_at_90  #rad

    for corner in corners:

        #Fx
        data_frame.loc[:, "WFT_Force_X_Vehicle_Body_"+corner] = data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_"+corner] * np.cos(steer_at_wheel) - data_frame.loc[:,"WFT_Force_Y_Tyre_Corrected_"+corner] * np.sin(steer_at_wheel)

        #Fy
        data_frame.loc[:, "WFT_Force_Y_Vehicle_Body_"+corner] = data_frame.loc[:, "WFT_Force_X_Tyre_Corrected_"+corner] * np.sin(steer_at_wheel) + data_frame.loc[:, "WFT_Force_Y_Tyre_Corrected_"+corner] * np.cos(steer_at_wheel)

        #Fz
        data_frame.loc[:, "WFT_Force_Z_Vehicle_Body_"+corner] = data_frame["WFT_Force_Z_Tyre_Corrected_"+corner]

        #Mx
        data_frame.loc[:, "WFT_Moment_X_Vehicle_Body_"+corner] = data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_"+corner] * np.cos(steer_at_wheel) - data_frame.loc[:,"WFT_Moment_Y_Tyre_Corrected_"+corner] * np.sin(steer_at_wheel)

        #My
        data_frame.loc[:, "WFT_Moment_Y_Vehicle_Body_"+corner] = data_frame.loc[:, "WFT_Moment_X_Tyre_Corrected_"+corner] * np.sin(steer_at_wheel) + data_frame.loc[:, "WFT_Moment_Y_Tyre_Corrected_"+corner] * np.cos(steer_at_wheel)

        #Mz
        data_frame.loc[:, "WFT_Moment_Z_Vehicle_Body_"+corner] = data_frame["WFT_Moment_Z_Tyre_Corrected_"+corner]


########################################################################################################################
# The following sections corresponds to the calculation of the Slip Angle.
def calculate_wheel_SA(data_frame_total):
    print("Calculating Slip Angles")
    for data_frame in data_frame_total:

        yaw_rate = np.radians(data_frame.loc[:, "ADMA Ang Velocity Body Z"]) #deg -> rad
        vx = data_frame.loc[:, "SAS3 S350 Velocity X"]/1000 #mm/s -> m/s
        vy = data_frame.loc[:, "SAS3 S350 Velocity Y"]/1000 #mm/s -> m/s
        steering = np.radians(data_frame.loc[:, 'Steered Angle']) #deg -> rad

        a = vehicle.get_front_wheelbase()
        b = vehicle.get_rear_wheelbase()
        front_track = vehicle.front_track
        rear_track = vehicle.rear_track
        steer_ratio = vehicle.steering_ratio_at_90

        slip_angle_FL = ((vy + yaw_rate * a) / (vx - yaw_rate * front_track/2)) - (steering / steer_ratio)
        slip_angle_FR = ((vy + yaw_rate * a) / (vx + yaw_rate * front_track/2)) - (steering / steer_ratio)
        slip_angle_RL = (vy - yaw_rate * b) / (vx - yaw_rate * rear_track/2)
        slip_angle_RR = (vy - yaw_rate * b) / (vx + yaw_rate * rear_track/2)

        data_frame.loc[:, "slip_angle_FL"] = slip_angle_FL
        data_frame.loc[:, "slip_angle_FR"] = slip_angle_FR
        data_frame.loc[:, "slip_angle_RL"] = slip_angle_RL
        data_frame.loc[:, "slip_angle_RR"] = slip_angle_RR

def normalize_time(data_frame_total):
    print("Normalizing time")
    for data_frame in data_frame_total:
        data_frame.loc[:, "Time"] = data_frame.loc[:, "Time"] - data_frame["Time"].iloc[0]

def calculate_yaw_angle_corrected(data_frame_total):
    print("Correcting Yaw Rate")
    for data_frame in data_frame_total:
        data_frame.loc[:, "Yaw Rate Corrected"] = -(data_frame.loc[:, "IMU IB-6 Angular Speed Z"]/0.36)
