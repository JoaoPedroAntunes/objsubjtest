import os
import time

import pandas as pd
from pandas import DataFrame

from ImportData import DataPosProcessor
from ImportData.CropTable import separate_data
from SetupFolder import GlobalTestFile
from os import listdir
from os.path import isfile, join

from SetupFolder.GlobalTestFile import get_crop_points


def import_test_data(test_name):
    df = import_motec_csv(GlobalTestFile.test_file[test_name])
    crop_points = get_crop_points(test_name)
    df_tests = separate_data(crop_points, df)
    DataPosProcessor.pos_process_WFT(df_tests)
    DataPosProcessor.calculate_wheel_SA(df_tests)
    DataPosProcessor.normalize_time(df_tests)
    return df_tests



def import_motec_csv(file_path) -> DataFrame:
    print("")
    print("")
    print("Reading Data: '" + file_path + "'")
    start_time = time.time()
    df_read = pd.read_csv(file_path, skiprows=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15])
    elapsed_time = time.time() - start_time
    print("Data sucessfully read. Time: " + str(elapsed_time))

    return df_read

def import_csv(test_file):
    print("")
    print("Importing Data: '" + test_file + "'")

    folder = GlobalTestFile.test_file[test_file]
    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    df_list = []

    for files in onlyfiles:
        df_list.append(pd.read_csv(folder + "\\" + files))
        print("Imported Data: -" + test_file + "\\" + files + "'")

    print("Done")
    return df_list


def save_data_as_csv(file_path, data, name):
    if os.path.exists(file_path) == False:
        os.makedirs(file_path)

    for i in range(len(data)):
        new_name = file_path + "\\" + name + "_test_" + str(i+10) + ".csv"  # It starts with 10 so that the files
                                                                            # are imported in the same order
        start_time = time.time()
        data[i].to_csv(new_name)
        elapsed_time = time.time() - start_time

        print("Created new CSV file at: '{0}'. Time: {1}".format(new_name,elapsed_time))
    print("Done")
