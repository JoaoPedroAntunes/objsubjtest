"""
Joao Antunes
This script will read all the csv value that are specified in the SetupFolder.GlobalTestFile.Raw list.
Once read it will separate the files into csv files based on what the user specified in the crops are
"""
import os
from ImportData import ImportTestData, DataPosProcessor, CropTable
from SetupFolder import GlobalTestFile


def process_data():
    # Loop all csv files.
    for raw_file in GlobalTestFile.raw_files:

        # Check if the file exists. If the files exists we don't want to process it again
        if os.path.exists(GlobalTestFile.test_file[raw_file]):
            print("The folder: '" + GlobalTestFile.test_file[raw_file] + "' already exists, skipping")

        # If file doesn't exist
        else:
            # Import Data
            # Read Motec csv file and save it as a data frame
            df = ImportTestData.import_motec_csv(GlobalTestFile.raw_files[raw_file])

            #Process Data
            # Based on the name of the file (raw_file) get the user list with the points to crop
            crop_points = GlobalTestFile.get_crop_points(raw_file)
            # Separate the motec csv file in multiple dataframe (1 per test). This was specified in the crop_points table
            df_cropped = CropTable.separate_data(crop_points, df)
            # Calculate the correct values of the wheel force transducer
            DataPosProcessor.pos_process_WFT(df_cropped)
            # Calculate the wheel slip angle based on the slip angle sensors
            DataPosProcessor.calculate_wheel_SA(df_cropped)
            # Normalize the time. This makes so that every test that we look at always says at 0.
            DataPosProcessor.normalize_time(df_cropped)
            # Calculate the correct yaw angle
            DataPosProcessor.calculate_yaw_angle_corrected(df_cropped)

            #Save the data
            ImportTestData.save_data_as_csv(GlobalTestFile.test_file[raw_file], df_cropped, raw_file)


if __name__ == "__main__":
    process_data()
