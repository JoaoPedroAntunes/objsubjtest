from ImportData.ImportTestData import import_motec_csv
from ImportData.CropTable import separate_data
import matplotlib.pyplot as plt
import forces
import math
#Defining a dictionary of tests to it's easier for me to know which test I'm using.
from ImportData import DataPosProcessor
import vehicle
import numpy as np
from SetupFolder import GlobalTestFile
from SetupFolder.GlobalTestFile import get_crop_points


def import_test_data(test_name):
    df = import_motec_csv(GlobalTestFile.test_file[test_name])
    crop_points = get_crop_points(test_name)
    df_cropped = separate_data(crop_points, df)
    DataPosProcessor.pos_process_WFT(df_cropped)
    DataPosProcessor.calculate_wheel_SA(df_cropped)
    DataPosProcessor.normalize_time(df_cropped)
    return df_cropped


df_cropped = import_test_data(GlobalTestFile.TEST_FILE_NAME)


selected_data = df_cropped[0]



"""
#The following section corresponds to processing the Yaw Moment and comparing it with the Gyro
Izz = vehicle.Izz
yaw_acc_IMU = np.diff(selected_data.loc[:, "ADMA Ang Velocity Body Z"])
dx = np.diff(selected_data.loc[:,"Time"])
yaw_acc_IMU = np.append([yaw_acc_IMU],[0])
dx = np.append([dx],[0])

yaw_moment_IMU = Izz * selected_data.loc[:, "ADMA Ang Velocity Body Z"] * math.radians(1)
_, ax = plt.subplots()
ax.plot(selected_data.loc[:,"Time"],yaw_moment)
ax.plot(selected_data.loc[:,"Time"],-yaw_moment_IMU)
ax.legend(["Yaw Moment"])
plt.show()
"""
