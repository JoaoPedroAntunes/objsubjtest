wheelbase = 2.7 #m
front_track = 1.547 #m
rear_track = 1.562 #m

total_weight_front = 832.9*9.81 #N
total_weight_rear = 542.5*9.81 #N

static_weight_FL = 410.8*9.81 #N
static_weight_FR = 422.1*9.81 #N
static_weight_RL = 267*9.81 #N
static_weight_RR = 275.5*9.81 #N

steering_ratio_at_0 = 15.7
steering_ratio_at_90 = 14.4

Izz = 50000

def get_total_weight():
    return total_weight_front + total_weight_rear

def get_weight_distribution():
    return total_weight_front/(total_weight_front + total_weight_rear)

def get_front_wheelbase():
    return wheelbase * get_weight_distribution()

def get_rear_wheelbase():
    return wheelbase * (1 - get_weight_distribution())