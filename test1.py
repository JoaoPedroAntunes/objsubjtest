"""
Joao Antunes
Calculate the understeer grandient for each of the tires
"""
import numpy as np
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import matplotlib.pyplot as plt
from scipy import signal



# Define matplotlib style
GlobalTestFile.set_matplotlib_style()

# Import data for skidpad in spec1 and spec2
df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)

# Select test to be plotted.
selected_data = df_tests[GlobalTestFile.TEST_NUMBER]

x_time_list = []
steering_wheel_angle_list = []

# Delete Bad tests.
del df_tests[19]
del df_tests[18]
del df_tests[17]
del df_tests[15]
del df_tests[12]
del df_tests[11]
del df_tests[10]
del df_tests[9]
del df_tests[5]
del df_tests[3]
del df_tests[2]
del df_tests[1]

for df in df_tests:
    x_time_list.append(df["Time"])
    steering_wheel_angle_list.append(df["Steered Angle"])




for i in range(len((steering_wheel_angle_list))): steering_wheel_angle_list[i]=steering_wheel_angle_list[i].abs()

_, ax = plt.subplots()
i=1
t_list = []
for x_time, steering_wheel_angle in zip(x_time_list,steering_wheel_angle_list):
    ax.plot(x_time, steering_wheel_angle)
    t_list.append("Test" + str(i))
    i=i+1

ax.legend(t_list)
plt.grid(True)
plt.xlabel("Time [s]")
plt.ylabel("Steering Wheel Angle [deg]")


#
test_14 = df_tests[13]
x_time = test_14["Time"]
steering_wheel_angle = test_14["Steered Angle"]
steering_wheel_angle.loc[test_14["Time"]>1.022] = 70
_, ax = plt.subplots()
ax.plot(x_time, np.abs(steering_wheel_angle))
plt.xlabel("Time [s]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.grid(True)


# Get all on the same point:
for i in range(len(steering_wheel_angle_list)):
    steering_wheel_angle_list[i] = steering_wheel_angle_list[i] - steering_wheel_angle_list[i][0]


_, ax = plt.subplots()
ax.plot(x_time_list[0], np.abs(steering_wheel_angle_list[0]))
plt.xlabel("Time [s]")
plt.ylabel("Steering Wheel Angle [deg]")
plt.grid(True)



for i in range(len(x_time_list)):
    nonzero_index = np.where(steering_wheel_angle_list[i]>2)
    nonzero_time = x_time_list[i][nonzero_index[0][0]]
    x_time_list[i] = x_time_list[i] - nonzero_time

# Normalize the Points
for i in range(len(steering_wheel_angle_list)):
    max_steer = np.max(steering_wheel_angle_list[i])
    steering_wheel_angle_list[i] = steering_wheel_angle_list[i]/max_steer