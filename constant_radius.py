import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Constant Radius\\Argentina 2018 Sunday Skid Pad.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())


#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()

plt.show()

time_crop_list = [42,92,132.96,180.33,219.75,270.33,309.93,349.78,394.15]
time_crop_right_list = [0,59.29,111.92,150.05,194.84,237.89,285.43,326.28,365.33]

#Create the threshold data
left_1 = df_raw_constant_radius['Time'] <= 42
right_1 = (df_raw_constant_radius['Time'] > 59) & (df_raw_constant_radius['Time'] <= 92)
left_2 = (df_raw_constant_radius['Time'] > 111) & (df_raw_constant_radius['Time'] <= 132)
right_2 = (df_raw_constant_radius['Time'] > 150) & (df_raw_constant_radius['Time'] <= 180)
left_3 = (df_raw_constant_radius['Time'] > 194) & (df_raw_constant_radius['Time'] <= 219)
right_3 = (df_raw_constant_radius['Time'] > 237) & (df_raw_constant_radius['Time'] <= 270)
left_4 = (df_raw_constant_radius['Time'] > 285) & (df_raw_constant_radius['Time'] <= 309)
right_4 = (df_raw_constant_radius['Time'] > 326) & (df_raw_constant_radius['Time'] <= 349)
left_5 = (df_raw_constant_radius['Time'] > 365) & (df_raw_constant_radius['Time'] <= 394)

#Get the data frame data with the treshold
#Left
df_left_1 = df_raw_constant_radius[left_1]
df_left_2 = df_raw_constant_radius[left_2]
df_left_3 = df_raw_constant_radius[left_3]
df_left_4 = df_raw_constant_radius[left_4]
df_left_5 = df_raw_constant_radius[left_5]

#Right
df_right_1 = df_raw_constant_radius[right_1]
df_right_2 = df_raw_constant_radius[right_2]
df_right_3 = df_raw_constant_radius[right_3]
df_right_4 = df_raw_constant_radius[right_4]

fig, axs = plt.subplots(5,2)
axs[0,0].plot(df_left_1.loc[:,"Time"], df_left_1.loc[:,"Steered Angle"])
axs[1,0].plot(df_left_2.loc[:,"Time"], df_left_2.loc[:,"Steered Angle"])
axs[2,0].plot(df_left_3.loc[:,"Time"], df_left_3.loc[:,"Steered Angle"])
axs[3,0].plot(df_left_4.loc[:,"Time"], df_left_4.loc[:,"Steered Angle"])
axs[4,0].plot(df_left_5.loc[:,"Time"], df_left_5.loc[:,"Steered Angle"])

axs[0,1].plot(df_right_1.loc[:,"Time"], df_right_1.loc[:,"Steered Angle"])
axs[1,1].plot(df_right_2.loc[:,"Time"], df_right_2.loc[:,"Steered Angle"])
axs[2,1].plot(df_right_3.loc[:,"Time"], df_right_3.loc[:,"Steered Angle"])
axs[3,1].plot(df_right_4.loc[:,"Time"], df_right_4.loc[:,"Steered Angle"])

axs[0, 0].legend(['Left Turn 1'])
axs[1, 0].legend(['Left Turn 2'])
axs[2, 0].legend(['Left Turn 3'])
axs[3, 0].legend(['Left Turn 4'])
axs[4, 0].legend(['Left Turn 5'])

axs[0, 1].legend(['Right Turn 1'])
axs[1, 1].legend(['Right Turn 2'])
axs[2, 1].legend(['Right Turn 3'])
axs[3, 1].legend(['Right Turn 4'])


## Plot all the left and right turns together
fig3, ax3 = plt.subplots(1,2)
ax3[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"Steered Angle"])
ax3[0].plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], df_left_2.loc[:,"Steered Angle"])
ax3[0].plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], df_left_3.loc[:,"Steered Angle"])
ax3[0].plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], df_left_4.loc[:,"Steered Angle"])
ax3[0].plot(df_left_5.loc[:,"Time"] - df_left_5.loc[df_left_5["Time"].index[0],"Time"], df_left_5.loc[:,"Steered Angle"])

ax3[1].plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"])
ax3[1].plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"])
ax3[1].plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"])
ax3[1].plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"])

ax3[0].set(xlabel='time (s)', ylabel='steering angle (deg)', title="Left Turn")
ax3[0].grid()
ax3[1].set(xlabel='time (s)', ylabel='steering angle (deg)', title = "Right Turn")
ax3[1].grid()

ax3[0].legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4", "Left Turn 5"])
ax3[1].legend(['Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4"])




#Apply absolute on the whole data
fig4, ax4 = plt.subplots(1,2, sharey=True)
ax4[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], np.abs(df_left_1.loc[:,"Steered Angle"]))
ax4[0].plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], np.abs(df_left_2.loc[:,"Steered Angle"]))
ax4[0].plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], np.abs(df_left_3.loc[:,"Steered Angle"]))
ax4[0].plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], np.abs(df_left_4.loc[:,"Steered Angle"]))
ax4[0].plot(df_left_5.loc[:,"Time"] - df_left_5.loc[df_left_5["Time"].index[0],"Time"], np.abs(df_left_5.loc[:,"Steered Angle"]))

ax4[1].plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"])
ax4[1].plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"])
ax4[1].plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"])
ax4[1].plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"])

ax4[0].set(xlabel='time (s)', ylabel='steering angle (deg)', title="Left Turn")
ax4[0].grid()
ax4[1].set(xlabel='time (s)', ylabel='steering angle (deg)', title = "Right Turn")
ax4[1].grid()
ax4[0].legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4", "Left Turn 5"])
ax4[1].legend(['Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4"])

#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], np.abs(df_left_1.loc[:,"Steered Angle"]))
ax5.plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], np.abs(df_left_2.loc[:,"Steered Angle"]))
ax5.plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], np.abs(df_left_3.loc[:,"Steered Angle"]))
ax5.plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], np.abs(df_left_4.loc[:,"Steered Angle"]))
ax5.plot(df_left_5.loc[:,"Time"] - df_left_5.loc[df_left_5["Time"].index[0],"Time"], np.abs(df_left_5.loc[:,"Steered Angle"]))

ax5.plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"],'-.')

ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Constant Radius Test")
ax5.legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4", "Left Turn 5",'Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4"])
ax5.grid()
