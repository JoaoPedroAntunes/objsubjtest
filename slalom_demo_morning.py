import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Hot Laps\\Argentina 2018 Wednesday Demo On-track Morning.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [1054.51,
                        1137.07,
                        1176.82,
                        1217.31,
                        1257.60,
                        1297.56]

time_crop_left_list = [1061.42,
                       1142.72,
                       1183.11,
                       1222.97,
                       1263.41,
                       1303.22]

#Create the threshold data
hot_lap_1 = (df_raw_constant_radius['Time'] > 1054) & (df_raw_constant_radius['Time'] <= 1061)
hot_lap_2 = (df_raw_constant_radius['Time'] > 1137) & (df_raw_constant_radius['Time'] <= 1142)
hot_lap_3 = (df_raw_constant_radius['Time'] > 1176) & (df_raw_constant_radius['Time'] <= 1183)
hot_lap_4 = (df_raw_constant_radius['Time'] > 1217) & (df_raw_constant_radius['Time'] <= 1222)
hot_lap_5 = (df_raw_constant_radius['Time'] > 1257) & (df_raw_constant_radius['Time'] <= 1263)
hot_lap_6 = (df_raw_constant_radius['Time'] > 1297) & (df_raw_constant_radius['Time'] <= 1303)



#Get the data frame data with the treshold
#Note here the data was recorded the other way around

df_hot_lap_1 = df_raw_constant_radius[hot_lap_1]
df_hot_lap_2 = df_raw_constant_radius[hot_lap_2]
df_hot_lap_3 = df_raw_constant_radius[hot_lap_3]
df_hot_lap_4 = df_raw_constant_radius[hot_lap_4]
df_hot_lap_5 = df_raw_constant_radius[hot_lap_5]
df_hot_lap_6 = df_raw_constant_radius[hot_lap_6]


fig, axs = plt.subplots(6)
axs[0].plot(df_hot_lap_1.loc[:,"Time"], df_hot_lap_1.loc[:,"Steered Angle"])
axs[1].plot(df_hot_lap_2.loc[:,"Time"], df_hot_lap_2.loc[:,"Steered Angle"])
axs[2].plot(df_hot_lap_3.loc[:,"Time"], df_hot_lap_3.loc[:,"Steered Angle"])
axs[3].plot(df_hot_lap_4.loc[:,"Time"], df_hot_lap_4.loc[:,"Steered Angle"])
axs[4].plot(df_hot_lap_5.loc[:,"Time"], df_hot_lap_5.loc[:,"Steered Angle"])
axs[5].plot(df_hot_lap_6.loc[:,"Time"], df_hot_lap_6.loc[:,"Steered Angle"])


axs[0].legend(['Slalom 1'])
axs[1].legend(['Slalom 2'])
axs[2].legend(['Slalom 3'])
axs[3].legend(['Slalom 4'])
axs[4].legend(['Slalom 5'])
axs[5].legend(['Slalom 6'])


#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_hot_lap_1.loc[:,"Time"] - df_hot_lap_1.loc[df_hot_lap_1["Time"].index[0],"Time"], df_hot_lap_1.loc[:,"Steered Angle"])
ax5.plot(df_hot_lap_2.loc[:,"Time"] - df_hot_lap_2.loc[df_hot_lap_2["Time"].index[0],"Time"], df_hot_lap_2.loc[:,"Steered Angle"])
ax5.plot(df_hot_lap_3.loc[:,"Time"] - df_hot_lap_3.loc[df_hot_lap_3["Time"].index[0],"Time"], df_hot_lap_3.loc[:,"Steered Angle"])
ax5.plot(df_hot_lap_4.loc[:,"Time"] - df_hot_lap_4.loc[df_hot_lap_4["Time"].index[0],"Time"], df_hot_lap_4.loc[:,"Steered Angle"])
ax5.plot(df_hot_lap_5.loc[:,"Time"] - df_hot_lap_5.loc[df_hot_lap_5["Time"].index[0],"Time"], df_hot_lap_5.loc[:,"Steered Angle"])
ax5.plot(df_hot_lap_6.loc[:,"Time"] - df_hot_lap_6.loc[df_hot_lap_6["Time"].index[0],"Time"], df_hot_lap_6.loc[:,"Steered Angle"])


ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Hot Laps")
ax5.legend(['Slalom 1',"Slalom 2", "Slalom  3", "Slalom 4","Slalom 5", "Slalom 6"])
ax5.grid()

