import vehicle


def sum_forces(data, dir):
    return data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_FL"] + data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_FR"] + data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_RL"] + data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_RR"]


def sum_moments(data, dir):
    return data.loc[:, "WFT_Moment_"+dir+"_Vehicle_Body_FL"] + data.loc[:, "WFT_Moment_"+dir+"_Vehicle_Body_FR"] + data.loc[:, "WFT_Moment_"+dir+"_Vehicle_Body_RL"] + data.loc[:, "WFT_Moment_"+dir+"_Vehicle_Body_RR"]

def get_force(data, dir, wheel):
    return data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_"+wheel]

def get_moment(data, dir, wheel):
    return data.loc[:, "WFT_Moment_"+dir+"_Vehicle_Body_"+wheel]

def get_sum_moments_fx(data):
    front_track = vehicle.front_track
    rear_track = vehicle.rear_track

    sum_moments_Fx_FL = get_force(data, "X", "FL")
    sum_moments_Fx_FR = get_force(data, "X", "FR")
    sum_moments_Fx_RL = get_force(data, "X", "RL")
    sum_moments_Fx_RR = get_force(data, "X", "RR")
    sum_moments_Fx = (sum_moments_Fx_FL * front_track / 2) - (sum_moments_Fx_FR * front_track / 2) + (
                sum_moments_Fx_RL * rear_track / 2) - (sum_moments_Fx_RR * rear_track / 2)

    return sum_moments_Fx

def get_sum_moments_fy(data):
    front_wheelbase = vehicle.get_front_wheelbase()  # a
    rear_wheelbase = vehicle.get_rear_wheelbase()  # b

    sum_moments_Fy_FL = get_force(data, "Y", "FL")
    sum_moments_Fy_FR = get_force(data, "Y", "FR")
    sum_moments_Fy_RL = get_force(data, "Y", "RL")
    sum_moments_Fy_RR = get_force(data, "Y", "RR")
    sum_moments_Fy = (sum_moments_Fy_FR * front_wheelbase) + (sum_moments_Fy_FL * front_wheelbase) - (
                sum_moments_Fy_RL * rear_wheelbase) - (sum_moments_Fy_RR * rear_wheelbase)

    return sum_moments_Fy

def get_sum_moments_mz(data):
    # Yaw moment contribution from Mz
    sum_moments_Mz_FL = get_moment(data, "Z", "FL")
    sum_moments_Mz_FR = get_moment(data, "Z", "FR")
    sum_moments_Mz_RL = get_moment(data, "Z", "RL")
    sum_moments_Mz_RR = get_moment(data, "Z", "RR")
    sum_moments_Mz = -(sum_moments_Mz_FL + sum_moments_Mz_FR + sum_moments_Mz_RL + sum_moments_Mz_RR)
    return sum_moments_Mz