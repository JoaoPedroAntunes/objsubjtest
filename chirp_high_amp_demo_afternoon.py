import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Chirp\\Argentina 2018 Wednesday Demo On-track Afternoon.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [299.13,
                        340.40,
                        405.91,
                        444.97,
                        510.74,
                        549.56]

time_crop_left_list = [317.00,
                       358.83,
                       422.87,
                       463.84,
                       527.95,
                       567.75]

#Create the threshold data
chirp_1 = (df_raw_constant_radius['Time'] > 299) & (df_raw_constant_radius['Time'] <= 317)
chirp_2 = (df_raw_constant_radius['Time'] > 340) & (df_raw_constant_radius['Time'] <= 358)
chirp_3 = (df_raw_constant_radius['Time'] > 405) & (df_raw_constant_radius['Time'] <= 422)
chirp_4 = (df_raw_constant_radius['Time'] > 444) & (df_raw_constant_radius['Time'] <= 463)
chirp_5 = (df_raw_constant_radius['Time'] > 510) & (df_raw_constant_radius['Time'] <= 527)
chirp_6 = (df_raw_constant_radius['Time'] > 549) & (df_raw_constant_radius['Time'] <= 567)



#Get the data frame data with the treshold
#Note here the data was recorded the other way around

df_chirp_1 = df_raw_constant_radius[chirp_1]
df_chirp_2 = df_raw_constant_radius[chirp_2]
df_chirp_3 = df_raw_constant_radius[chirp_3]
df_chirp_4 = df_raw_constant_radius[chirp_4]
df_chirp_5 = df_raw_constant_radius[chirp_5]
df_chirp_6 = df_raw_constant_radius[chirp_6]



fig, axs = plt.subplots(6)
axs[0].plot(df_chirp_1.loc[:,"Time"], df_chirp_1.loc[:,"Steered Angle"])
axs[1].plot(df_chirp_2.loc[:,"Time"], df_chirp_2.loc[:,"Steered Angle"])
axs[2].plot(df_chirp_3.loc[:,"Time"], df_chirp_3.loc[:,"Steered Angle"])
axs[3].plot(df_chirp_4.loc[:,"Time"], df_chirp_4.loc[:,"Steered Angle"])
axs[4].plot(df_chirp_5.loc[:,"Time"], df_chirp_5.loc[:,"Steered Angle"])
axs[5].plot(df_chirp_6.loc[:,"Time"], df_chirp_6.loc[:,"Steered Angle"])



axs[0].legend(['Chirp 1'])
axs[1].legend(['Chirp 2'])
axs[2].legend(['Chirp 3'])
axs[3].legend(['Chirp 4'])
axs[4].legend(['Chirp 5'])
axs[5].legend(['Chirp 6'])



#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_chirp_1.loc[:,"Time"] - df_chirp_1.loc[df_chirp_1["Time"].index[0],"Time"], df_chirp_1.loc[:,"Steered Angle"])
ax5.plot(df_chirp_2.loc[:,"Time"] - df_chirp_2.loc[df_chirp_2["Time"].index[0],"Time"], df_chirp_2.loc[:,"Steered Angle"])
ax5.plot(df_chirp_3.loc[:,"Time"] - df_chirp_3.loc[df_chirp_3["Time"].index[0],"Time"], df_chirp_3.loc[:,"Steered Angle"])
ax5.plot(df_chirp_4.loc[:,"Time"] - df_chirp_4.loc[df_chirp_4["Time"].index[0],"Time"], df_chirp_4.loc[:,"Steered Angle"])
ax5.plot(df_chirp_5.loc[:,"Time"] - df_chirp_5.loc[df_chirp_5["Time"].index[0],"Time"], df_chirp_5.loc[:,"Steered Angle"])
ax5.plot(df_chirp_6.loc[:,"Time"] - df_chirp_6.loc[df_chirp_6["Time"].index[0],"Time"], df_chirp_6.loc[:,"Steered Angle"])



ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Chirp")
ax5.legend(['Chirp 1',"Chirp 2", "Chirp  3", "Chirp 4","Chirp 5", "Chirp 6"])
ax5.grid()

