import  WFT_Correction
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.signal
import vehicle

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Constant Radius\\Argentina 2018 Monday Skidpad 11.29 am Spec 2.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [28.4,
                        70.49,
                        113.86,
                        155.72,
                        205.93,
                        245.82,
                        286.91,
                        328.73,
                        370.88,
                        407.77]

time_crop_left_list = [0,
                       42.58,
                       86.54,
                       129.62,
                       172.94,
                       221.88,
                       260.08,
                       342.45,
                       384.51]

#Create the threshold data
left_1 = df_raw_constant_radius['Time'] <= 28
right_1 = (df_raw_constant_radius['Time'] > 42) & (df_raw_constant_radius['Time'] <= 70)
left_2 = (df_raw_constant_radius['Time'] > 86) & (df_raw_constant_radius['Time'] <= 113)
right_2 = (df_raw_constant_radius['Time'] > 129) & (df_raw_constant_radius['Time'] <= 155)
left_3 = (df_raw_constant_radius['Time'] > 172) & (df_raw_constant_radius['Time'] <= 205)
right_3 = (df_raw_constant_radius['Time'] > 221) & (df_raw_constant_radius['Time'] <= 245)
left_4 = (df_raw_constant_radius['Time'] > 260) & (df_raw_constant_radius['Time'] <= 286)
right_4 = (df_raw_constant_radius['Time'] > 302) & (df_raw_constant_radius['Time'] <= 328)
left_5 = (df_raw_constant_radius['Time'] > 342) & (df_raw_constant_radius['Time'] <= 370)
right_5 = (df_raw_constant_radius['Time'] > 384) & (df_raw_constant_radius['Time'] <= 407)


#Get the data frame data with the treshold
#Note here the data was recorded the other way around
#Left
df_left_1 = df_raw_constant_radius[right_1]
df_left_2 = df_raw_constant_radius[right_2]
df_left_3 = df_raw_constant_radius[right_3]
df_left_4 = df_raw_constant_radius[right_4]
df_left_5 = df_raw_constant_radius[right_5]

#Right
df_right_1 = df_raw_constant_radius[left_1]
df_right_2 = df_raw_constant_radius[left_2]
df_right_3 = df_raw_constant_radius[left_3]
df_right_4 = df_raw_constant_radius[left_4]
df_right_5 = df_raw_constant_radius[left_5]


fig, axs = plt.subplots(5,2)
axs[0,0].plot(df_left_1.loc[:,"Time"], df_left_1.loc[:,"Steered Angle"])
axs[1,0].plot(df_left_2.loc[:,"Time"], df_left_2.loc[:,"Steered Angle"])
axs[2,0].plot(df_left_3.loc[:,"Time"], df_left_3.loc[:,"Steered Angle"])
axs[3,0].plot(df_left_4.loc[:,"Time"], df_left_4.loc[:,"Steered Angle"])
axs[4,0].plot(df_left_5.loc[:,"Time"], df_left_5.loc[:,"Steered Angle"])

axs[0,1].plot(df_right_1.loc[:,"Time"], df_right_1.loc[:,"Steered Angle"])
axs[1,1].plot(df_right_2.loc[:,"Time"], df_right_2.loc[:,"Steered Angle"])
axs[2,1].plot(df_right_3.loc[:,"Time"], df_right_3.loc[:,"Steered Angle"])
axs[3,1].plot(df_right_4.loc[:,"Time"], df_right_4.loc[:,"Steered Angle"])
axs[4,1].plot(df_right_5.loc[:,"Time"], df_right_5.loc[:,"Steered Angle"])

axs[0, 0].legend(['Left Turn 1'])
axs[1, 0].legend(['Left Turn 2'])
axs[2, 0].legend(['Left Turn 3'])
axs[3, 0].legend(['Left Turn 4'])
axs[4, 0].legend(['Left Turn 5'])

axs[0, 1].legend(['Right Turn 1'])
axs[1, 1].legend(['Right Turn 2'])
axs[2, 1].legend(['Right Turn 3'])
axs[3, 1].legend(['Right Turn 4'])
axs[3, 1].legend(['Right Turn 5'])


## Plot all the left and right turns together
fig3, ax3 = plt.subplots(1,2)
ax3[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"Steered Angle"])
ax3[0].plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], df_left_2.loc[:,"Steered Angle"])
ax3[0].plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], df_left_3.loc[:,"Steered Angle"])
ax3[0].plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], df_left_4.loc[:,"Steered Angle"])
ax3[0].plot(df_left_5.loc[:,"Time"] - df_left_5.loc[df_left_5["Time"].index[0],"Time"], df_left_5.loc[:,"Steered Angle"])

ax3[1].plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"])
ax3[1].plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"])
ax3[1].plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"])
ax3[1].plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"])
ax3[1].plot(df_right_5.loc[:,"Time"] - df_right_5.loc[df_right_5["Time"].index[0],"Time"], df_right_5.loc[:,"Steered Angle"])

ax3[0].set(xlabel='time (s)', ylabel='steering angle (deg)', title="Left Turn")
ax3[0].grid()
ax3[1].set(xlabel='time (s)', ylabel='steering angle (deg)', title = "Right Turn")
ax3[1].grid()

ax3[0].legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4", "Left Turn 5"])
ax3[1].legend(['Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4", "Right Turn 5"])



#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], np.abs(df_left_1.loc[:,"Steered Angle"]))
ax5.plot(df_left_2.loc[:,"Time"] - df_left_2.loc[df_left_2["Time"].index[0],"Time"], np.abs(df_left_2.loc[:,"Steered Angle"]))
ax5.plot(df_left_3.loc[:,"Time"] - df_left_3.loc[df_left_3["Time"].index[0],"Time"], np.abs(df_left_3.loc[:,"Steered Angle"]))
ax5.plot(df_left_4.loc[:,"Time"] - df_left_4.loc[df_left_4["Time"].index[0],"Time"], np.abs(df_left_4.loc[:,"Steered Angle"]))
ax5.plot(df_left_5.loc[:,"Time"] - df_left_5.loc[df_left_5["Time"].index[0],"Time"], np.abs(df_left_5.loc[:,"Steered Angle"]))

ax5.plot(df_right_1.loc[:,"Time"] - df_right_1.loc[df_right_1["Time"].index[0],"Time"], df_right_1.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_2.loc[:,"Time"] - df_right_2.loc[df_right_2["Time"].index[0],"Time"], df_right_2.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_3.loc[:,"Time"] - df_right_3.loc[df_right_3["Time"].index[0],"Time"], df_right_3.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_4.loc[:,"Time"] - df_right_4.loc[df_right_4["Time"].index[0],"Time"], df_right_4.loc[:,"Steered Angle"],'-.')
ax5.plot(df_right_5.loc[:,"Time"] - df_right_5.loc[df_right_5["Time"].index[0],"Time"], df_right_5.loc[:,"Steered Angle"],'-.')

ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Constant Radius Test")
ax5.legend(['Left Turn 1',"Left Turn 2", "Left Turn 3", "Left Turn 4", "Left Turn 5",'Right Turn 1',"Right Turn 2", "Right Turn 3", "Right Turn 4", "Right Turn 5"])
ax5.grid()



#Plot Forces
fig = plt.figure(tight_layout=True)
gs = gridspec.GridSpec(3, 2)
ax = fig.add_subplot(gs[0, :])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"Steered Angle"])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"Vehicle Speed"])
ax.legend(["Steering Wheel Angle","Vehicle Speed"])

ax = fig.add_subplot(gs[1, 0])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel FL"],color="darkred")
ax = fig.add_subplot(gs[1, 1])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel FR"],color="darkgreen")

ax = fig.add_subplot(gs[2, 0])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel RL"],color="darkblue")

ax = fig.add_subplot(gs[2, 1])
ax.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel RR"],color="orange")

#Plot Forces but filter the signal:
fig5, ax5 = plt.subplots()
ax5.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel FL"])
ax5.plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], scipy.signal.savgol_filter(df_left_1.loc[:,"WFT Force X Wheel FL"],51,2),color="darkred")



def plot_forces(data, x, y1, y2, y3, y4, y5, y6):
    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(3, 2)
    ax = fig.add_subplot(gs[0, :])
    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y1])

    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y2])
    ax.legend([y1, y2])

    ax = fig.add_subplot(gs[1, 0])
    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y3], color="darkred")
    ax.legend([y3])

    ax = fig.add_subplot(gs[1, 1])
    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y4], color="darkgreen")
    ax.legend([y4])

    ax = fig.add_subplot(gs[2, 0])
    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y5], color="darkblue")
    ax.legend([y5])

    ax = fig.add_subplot(gs[2, 1])
    ax.plot(data.loc[:, x] - data.loc[data[x].index[0], x],
            data.loc[:, y6], color="orange")
    ax.legend([y6])

#Forces
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Force X Wheel FL","WFT Force X Wheel FR","WFT Force X Wheel RL","WFT Force X Wheel RR")
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Force Y Wheel FL","WFT Force Y Wheel FR","WFT Force Y Wheel RL","WFT Force Y Wheel RR")
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Force Z Wheel FL","WFT Force Z Wheel FR","WFT Force Z Wheel RL","WFT Force Z Wheel RR")

#Moments
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Moment X Wheel FL","WFT Moment X Wheel FR","WFT Moment X Wheel RL","WFT Moment X Wheel RR")
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Moment Y Wheel FL","WFT Moment Y Wheel FR","WFT Moment Y Wheel RL","WFT Moment Y Wheel RR")
plot_forces(df_left_1,"Time","Steered Angle","Vehicle Speed","WFT Moment Z Wheel FL","WFT Moment Z Wheel FR","WFT Moment Z Wheel RL","WFT Moment Z Wheel RR")


WFT_Correction.pos_process_WFT(df_left_1)



# Comparison Between Adjusting the signals or not
fig5, ax5 = plt.subplots(6)
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel FL"])
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_X_Tyre_Corrected_FL"])

ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Y Wheel FL"])
ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Y_Tyre_Corrected_FL"])

ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Z Wheel FL"])
ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Z_Tyre_Corrected_FL"])

ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment X Wheel FL"])
ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_X_Tyre_Corrected_FL"])

ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Y Wheel FL"])
ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Y_Tyre_Corrected_FL"])

ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Z Wheel FL"])
ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Z_Tyre_Corrected_FL"])


fig5, ax5 = plt.subplots(6)
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel FR"])
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_X_Tyre_Corrected_FR"])

ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Y Wheel FR"])
ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Y_Tyre_Corrected_FR"])

ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Z Wheel FR"])
ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Z_Tyre_Corrected_FR"])

ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment X Wheel FR"])
ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_X_Tyre_Corrected_FR"])

ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Y Wheel FR"])
ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Y_Tyre_Corrected_FR"])

ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Z Wheel FR"])
ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Z_Tyre_Corrected_FR"])


fig5, ax5 = plt.subplots(6)
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel RL"])
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_X_Tyre_Corrected_RL"])

ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Y Wheel RL"])
ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Y_Tyre_Corrected_RL"])

ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Z Wheel RL"])
ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Z_Tyre_Corrected_RL"])

ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment X Wheel RL"])
ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_X_Tyre_Corrected_RL"])

ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Y Wheel RL"])
ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Y_Tyre_Corrected_RL"])

ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Z Wheel RL"])
ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Z_Tyre_Corrected_RL"])


fig5, ax5 = plt.subplots(6)
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force X Wheel RR"])
ax5[0].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_X_Tyre_Corrected_RR"])

ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Y Wheel RR"])
ax5[1].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Y_Tyre_Corrected_RR"])

ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Force Z Wheel RR"])
ax5[2].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Force_Z_Tyre_Corrected_RR"])

ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment X Wheel RR"])
ax5[3].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_X_Tyre_Corrected_RR"])

ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Y Wheel RR"])
ax5[4].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Y_Tyre_Corrected_RR"])

ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT Moment Z Wheel RR"])
ax5[5].plot(df_left_1.loc[:,"Time"] - df_left_1.loc[df_left_1["Time"].index[0],"Time"], df_left_1.loc[:,"WFT_Moment_Z_Tyre_Corrected_RR"])

plt.show()