from os import listdir
from os.path import isfile, join
import pandas as pd
import matplotlib.pyplot as plt
from ImportData.ImportTestData import import_csv
from SetupFolder import GlobalTestFile
import os
from Report.SlipAngles import report_SA
from Report.DriverAnalysis import DriverInputs
from Report.Spec1_Spec2.Skidpad import RollAngle
# Edit here to change the name of the file that is going to be read
TEST_FILE_NAME = "sunday_step_steer_spec1"

folder = GlobalTestFile.test_file[TEST_FILE_NAME]
onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
df_list = []

df_tests = import_csv(GlobalTestFile.TEST_FILE_NAME)
for i in range(len(onlyfiles)):
    GlobalTestFile.TEST_NUMBER = i
    #report_SA.run_report(df_tests)
    #DriverInputs.run_report(df_tests)
    #RollAngle.run_report(df_tests)
plt.show()


