import  WFT_Correction
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.signal
import vehicle


def plot_forces(data, dir, title_text):
    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(3, 2)
    ax = fig.add_subplot(gs[0, :])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "Steered Angle"])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "Vehicle Speed"])
    plt.title(title_text)
    ax.legend(["Steering Wheel Angle", "Vehicle Speed"])
    plt.ylabel("deg & Km/h")
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(gs[1, 0])
    ax.plot(data.loc[:, "Time"]- data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Tyre_Corrected_FL"], color="darkred")
    ax.legend(["FL - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[1, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Tyre_Corrected_FR"], color="darkgreen")
    ax.legend(["FR - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[2, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Tyre_Corrected_RL"], color="darkblue")
    ax.legend(["RL - F" +dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[2, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Tyre_Corrected_RR"], color="orange")
    ax.legend(["RR - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")



def plot_moments(data, dir, title_text):
    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(3, 2)
    ax = fig.add_subplot(gs[0, :])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"], data.loc[:, "Steered Angle"])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"], data.loc[:, "Vehicle Speed"])
    plt.title(title_text)
    ax.legend(["Steering Wheel Angle", "Vehicle Speed"])
    plt.ylabel("deg & Km/h")
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(gs[1, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Tyre_Corrected_FL"], color="darkred")
    ax.legend(["FL - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[1, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Tyre_Corrected_FR"], color="darkgreen")
    ax.legend(["FR - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[2, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Tyre_Corrected_RL"], color="darkblue")
    ax.legend(["RL - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[2, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Tyre_Corrected_RR"], color="orange")
    ax.legend(["RR - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")


def plot_forces_at_vehicle_body(data, dir, title_text):
    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(3, 2)
    ax = fig.add_subplot(gs[0, :])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "Steered Angle"])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "Vehicle Speed"])
    plt.title(title_text)
    ax.legend(["Steering Wheel Angle", "Vehicle Speed"])
    plt.ylabel("deg & Km/h")
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(gs[1, 0])
    ax.plot(data.loc[:, "Time"]- data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_FL"], color="darkred")
    ax.legend(["FL - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[1, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_FR"], color="darkgreen")
    ax.legend(["FR - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[2, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_RL"], color="darkblue")
    ax.legend(["RL - F" +dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")

    ax = fig.add_subplot(gs[2, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0],"Time"], data.loc[:, "WFT_Force_"+dir+"_Vehicle_Body_RR"], color="orange")
    ax.legend(["RR - F"+dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Force (N)")



def plot_moments_at_vehicle_body(data, dir, title_text):
    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(3, 2)
    ax = fig.add_subplot(gs[0, :])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"], data.loc[:, "Steered Angle"])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"], data.loc[:, "Vehicle Speed"])
    plt.title(title_text)
    ax.legend(["Steering Wheel Angle", "Vehicle Speed"])
    plt.ylabel("deg & Km/h")
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(gs[1, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Vehicle_Body_FL"], color="darkred")
    ax.legend(["FL - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[1, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Vehicle_Body_FR"], color="darkgreen")
    ax.legend(["FR - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[2, 0])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Vehicle_Body_RL"], color="darkblue")
    ax.legend(["RL - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")

    ax = fig.add_subplot(gs[2, 1])
    ax.plot(data.loc[:, "Time"] - data.loc[data["Time"].index[0], "Time"],
            data.loc[:, "WFT_Moment_" + dir + "_Vehicle_Body_RR"], color="orange")
    ax.legend(["RR - M" + dir.lower()])
    plt.xlabel("Time (s)")
    plt.ylabel("Moment (Nm)")


def plot_forces_and_moments(data, title):
    plot_forces(data, "X", title + " - Fx")
    plot_forces(data, "Y", title + " - Fy")
    plot_forces(data, "Z", title + " - Fz")

    plot_moments(data, "X", title + " - Mx")
    plot_moments(data, "Y", title + " - My")
    plot_moments(data, "Z", title + " - Mz")


def plot_forces_and_moments_at_vehicle_body(data, title):
    plot_forces_at_vehicle_body(data, "X", title + " - Fx")
    plot_forces_at_vehicle_body(data, "Y", title + " - Fy")
    plot_forces_at_vehicle_body(data, "Z", title + " - Fz")

    plot_moments_at_vehicle_body(data, "X", title + " - Mx")
    plot_moments_at_vehicle_body(data, "Y", title + " - My")
    plot_moments_at_vehicle_body(data, "Z", title + " - Mz")
