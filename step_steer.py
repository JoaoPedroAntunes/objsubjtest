import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
df_spec2_hs_left_2 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 highspeed LEFT 2.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

df_spec2_hs_left_1 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 highspeed LEFT.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

df_spec2_hs_right_1 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 highspeed RIGHT.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

df_spec2_ls_left_1 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 lowspeed LEFT 1.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

df_spec2_ls_left_2 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 lowspeed LEFT 2.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

df_spec2_ls_right_1 = pd.read_csv("C:\\Users\\joaop\\Desktop\\Step Steer\\Step steer Spec 2 lowspeed RIGHT.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])

#Append the data to a list
raw_data = {
    "spec2_hs_left_1": df_spec2_hs_left_1,
    "spec2_hs_left_2": df_spec2_hs_left_2,

    "spec2_ls_left_1": df_spec2_ls_left_1,
    "spec2_ls_left_2": df_spec2_ls_left_2,

    "spec2_ls_right_1": df_spec2_ls_right_1,
    "spec2_hs_right_1": df_spec2_hs_right_1,
}

#Todo improve the code so that it's not necessary to have the [1] there.
for df in raw_data.items():
    print(list(df[1]))

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(raw_data["spec2_ls_left_1"].loc[:,"Time"], raw_data["spec2_ls_left_1"].loc[:,"Alias Steering Wheel Angle"])
ax.plot(raw_data["spec2_ls_left_2"].loc[:,"Time"], raw_data["spec2_ls_left_2"].loc[:,"Alias Steering Wheel Angle"])

ax.plot(raw_data["spec2_hs_left_1"].loc[:,"Time"], raw_data["spec2_hs_left_1"].loc[:,"Alias Steering Wheel Angle"])
ax.plot(raw_data["spec2_hs_left_2"].loc[:,"Time"], raw_data["spec2_hs_left_2"].loc[:,"Alias Steering Wheel Angle"])

ax.plot(raw_data["spec2_ls_right_1"].loc[:,"Time"], np.abs(raw_data["spec2_ls_right_1"].loc[:,"Alias Steering Wheel Angle"]))
ax.plot(raw_data["spec2_hs_right_1"].loc[:,"Time"], np.abs(raw_data["spec2_hs_right_1"].loc[:,"Alias Steering Wheel Angle"]))
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.legend(["spec2_ls_left_1","spec2_ls_left_2","spec2_hs_left_1","spec2_hs_left_2","spec2_ls_right_1","spec2_hs_right_1"])
ax.grid()

plt.show()

