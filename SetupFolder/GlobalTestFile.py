"""
Joao Antunes
The following script defines all the global variables used in this project.
If this is to be reused for other projects the only thing that needs to be done is to edit this file
"""
import matplotlib

# Dictionary with the name of the tests.
test_file = {
    "slalom": "C:\\Users\\joaop\\Desktop\\Hot Laps\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "Skidpad_spec1": "C:\\Users\\joaop\\Desktop\\Constant Radius\\Argentina 2018 Sunday Skid Pad.csv",
    "Skidpad_spec2": "C:\\Users\\joaop\\Desktop\\Constant Radius\\Argentina 2018 Monday Skidpad 11.29 am Spec 2.csv",
    "Hot Laps": "C:\\Users\\joaop\\Desktop\\Hot Laps\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "sunday_step_steer_spec1": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Sunday Spec 1\\Step Steer",
    "sunday_skidpad_spec1": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Sunday Spec 1\\Skidpad",
    "monday_skidpad_spec2": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Monday Spec 2\\Skidpad",
    "wednesday_afternoon_chirp_high": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Wednesday Afternoon\\Chirp High",
    "wednesday_afternoon_chirp_low": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Wednesday Afternoon\\Chirp Low",
    "wednesday_morning_skidpad": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Wednesday Morning\\Skidpad",
    "wednesday_morning_hot_laps": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Wednesday Morning\\Hot Laps",
    "wednesday_morning_slalom": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Wednesday Morning\\Slalom",
    "monday_step_steer_spec2": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Monday Spec 2\\Step Steer",
    "wednesday_morning_step_steer": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\Wednesday Morning\\Step Steer",
    "thursday_SA_comparison_skidpad": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Thursday SA\\Skidpad",
    "thursday_SA_comparison_low_chirp": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Thursday SA\\Low Chirp",
    "thursday_SA_comparison_high_chirp": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Thursday SA\\High Chirp",
    "thursday_SA_comparison_step_steer": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\Thursday SA\\Step Steer"
}

raw_files = {
    "sunday_step_steer_spec1": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\02_Sunday Evening Spec 1\\Argentina 2018 Sunday Step Steer 5.55 pm.csv",
    "sunday_skidpad_spec1": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\02_Sunday Evening Spec 1\\Argentina 2018 Sunday Skid Pad 5.30 pm.csv",
    "monday_skidpad_spec2": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\03_Monday Spec 2\\Argentina 2018 Monday Skidpad 11.29 am Spec 2.csv",
    "wednesday_afternoon_chirp_high": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Afternoon.csv",
    "wednesday_afternoon_chirp_low": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Afternoon.csv",
    "wednesday_morning_skidpad": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "wednesday_morning_hot_laps": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "wednesday_morning_slalom": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "wednesday_morning_step_steer": "C:\\Users\\joaop\Desktop\\Logged Data to CSV\\05_Wednesday Demo\\Argentina 2018 Wednesday Demo On-track Morning.csv",
    "thursday_SA_comparison_skidpad": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\06_Thursday Friday Additional Testing\\Argentina 2018 Thursday SA sensor comparison.csv",
    "thursday_SA_comparison_low_chirp": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\06_Thursday Friday Additional Testing\\Argentina 2018 Thursday SA sensor comparison.csv",
    "thursday_SA_comparison_high_chirp": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\06_Thursday Friday Additional Testing\\Argentina 2018 Thursday SA sensor comparison.csv",
    "thursday_SA_comparison_step_steer": "C:\\Users\\joaop\\Desktop\\Logged Data to CSV\\06_Thursday Friday Additional Testing\\Argentina 2018 Thursday SA sensor comparison.csv"
}
# Edit here to change the name of the file that is going to be read
TEST_FILE_NAME = "thursday_SA_comparison_skidpad"

# Change the number to analyze different test within the same file.
TEST_NUMBER = 0

def get_crop_points(test_file_name: str) -> list:
    """
    Function that saves the points where the data will be cropped. These points are time units.
    :param test_file_name: string with the name of the test
    :return: crop_points
    """
    crop_points = []
    if test_file_name == "sunday_step_steer_spec1":
        crop_points.append([33, 35])
        crop_points.append([72, 74])
        crop_points.append([140, 143])
        crop_points.append([179, 183])
        crop_points.append([233, 237])
        crop_points.append([272, 275])
        crop_points.append([325, 328])
        crop_points.append([361, 365])
        crop_points.append([414, 417])
        crop_points.append([452, 456])
        crop_points.append([502, 505])
        crop_points.append([536, 539])
        crop_points.append([583, 586])
        crop_points.append([618, 621])
        crop_points.append([666, 668])
        crop_points.append([702, 706])
        crop_points.append([749, 751])
        crop_points.append([784, 787])
        crop_points.append([832, 834])
        crop_points.append([866, 869])

    elif test_file_name == "monday_skidpad_spec2":
        crop_points.append([0, 28])
        crop_points.append([42, 70])
        crop_points.append([86, 113])
        crop_points.append([129, 155])
        crop_points.append([172, 205])
        crop_points.append([221, 245])
        crop_points.append([260, 286])
        crop_points.append([302, 308])
        crop_points.append([342, 370])
        crop_points.append([384, 407])

    elif test_file_name == "sunday_skidpad_spec1":
        crop_points.append([0, 42])
        crop_points.append([59, 92])
        crop_points.append([111, 132])
        crop_points.append([150, 180])
        crop_points.append([194, 219])
        crop_points.append([237, 270])
        crop_points.append([285, 309])
        crop_points.append([326, 349])
        crop_points.append([365, 394])
        crop_points.append([384, 407])

    elif test_file_name == "wednesday_afternoon_chirp_high":
        crop_points.append([299.13, 317.00])
        crop_points.append([340.40, 358.83])
        crop_points.append([405.91, 422.87])
        crop_points.append([444.97, 463.84])
        crop_points.append([510.74, 527.95])
        crop_points.append([549.56, 567.75])

    elif test_file_name == "wednesday_afternoon_chirp_low":
        crop_points.append([15.52, 42.18])
        crop_points.append([62.31, 87.89])
        crop_points.append([108.45, 125.31])
        crop_points.append([144.10, 160.65])
        crop_points.append([182.93, 200.25])

    elif test_file_name == "wednesday_morning_skidpad":
        crop_points.append([13.78, 49.44])
        crop_points.append([67.71, 104.30])
        crop_points.append([120.20, 161.74])
        crop_points.append([177.97, 215.05])
        crop_points.append([229.23, 269.97])
        crop_points.append([287.77, 325.79])
        crop_points.append([341.16, 370.06])
        crop_points.append([386.10, 415.75])

    elif test_file_name == "wednesday_morning_hot_laps":
        crop_points.append([528.77, 584.58])
        crop_points.append([617.96, 683.23])
        crop_points.append([726.80, 778.90])
        crop_points.append([811.63, 863.60])
        crop_points.append([899.40, 955.69])

    elif test_file_name == "wednesday_morning_slalom":
        crop_points.append([1054.51, 1061.42])
        crop_points.append([1137.07, 1142.72])
        crop_points.append([1176.92, 1183.11])
        crop_points.append([1217.31, 1222.97])
        crop_points.append([1257.60, 1263.41])
        crop_points.append([1297.56, 1303.22])

    elif test_file_name == "monday_step_steer_spec2":
        crop_points.append([27.574, 30.747])
        crop_points.append([65.081, 69.541])
        crop_points.append([123.109, 126.568])
        crop_points.append([160.353,164.724 ])
        crop_points.append([215.484 , 219.466])
        crop_points.append([252.976 , 257.884])
        crop_points.append([305.759, 309.556])
        crop_points.append([342.352, 346.698])
        crop_points.append([394.547, 398.357])
        crop_points.append([431.004, 435.109])
        crop_points.append([495.397, 498.352])
        crop_points.append([495.397, 498.352])
        crop_points.append([529.016, 532.455])
        crop_points.append([576.827, 579.999])
        crop_points.append([610.165, 613.905])
        crop_points.append([657.274, 659.827])
        crop_points.append([690.970, 694.639])
        crop_points.append([738.756, 742.176])
        crop_points.append([772.528, 776.305])
        crop_points.append([821.392, 824.679])
        crop_points.append([854.953, 858.667])

    elif  test_file_name == "thursday_SA_comparison_skidpad":
        crop_points.append([73.30, 104.85])

    elif test_file_name == "thursday_SA_comparison_low_chirp":
        crop_points.append([130.12, 143.64])

    elif test_file_name == "thursday_SA_comparison_high_chirp":
        crop_points.append([192, 212])

    elif test_file_name == "thursday_SA_comparison_step_steer":
        crop_points.append([216, 219])

    elif test_file_name == "wednesday_morning_step_steer":
        crop_points.append([531.51, 535.38])

    else:
        pass #TODO add an excpetion here
    return crop_points


def set_matplotlib_style() -> None:
    """
    Use this function to edit the template used to generate the charts for matplotlib
    :return: Nothing
    """
    matplotlib.rc('xtick', labelsize=12)
    matplotlib.rc('ytick', labelsize=12)
    matplotlib.rc('legend', fontsize="x-large")
    matplotlib.rc('axes', labelsize="x-large")


# Create Color Pallete
color_pallete_dark = ["darkred", "darkgreen"]
color_pallete_light = ["red", "lightgreen"]