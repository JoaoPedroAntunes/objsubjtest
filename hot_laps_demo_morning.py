import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Import data with pandas
print("Reading Data...")
df_raw_constant_radius = pd.read_csv("C:\\Users\\joaop\\Desktop\\Hot Laps\\Argentina 2018 Wednesday Demo On-track Morning.csv",
                               skiprows=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15])
print("Data sucessfully read")

print(df_raw_constant_radius.head())

#Plot Steering angle versus time for a LEFT turn
fig, ax = plt.subplots()
ax.plot(df_raw_constant_radius.loc[:,"Time"], df_raw_constant_radius.loc[:,"Steered Angle"])
ax.set(xlabel='time (s)', ylabel='steering angle (deg)')
ax.grid()


time_crop_right_list = [528.77,
                        617.96,
                        726.80,
                        811.63,
                        899.40]

time_crop_left_list = [584.68,
                       683.23,
                       778.90,
                       863.60,
                       955.69]

#Create the threshold data
hot_lap_1 = (df_raw_constant_radius['Time'] > 528) & (df_raw_constant_radius['Time'] <= 584)
hot_lap_2 = (df_raw_constant_radius['Time'] > 617) & (df_raw_constant_radius['Time'] <= 683)
hot_lap_3 = (df_raw_constant_radius['Time'] > 726) & (df_raw_constant_radius['Time'] <= 778)
hot_lap_4 = (df_raw_constant_radius['Time'] > 811) & (df_raw_constant_radius['Time'] <= 863)
hot_lap_5 = (df_raw_constant_radius['Time'] > 899) & (df_raw_constant_radius['Time'] <= 955)



#Get the data frame data with the treshold
#Note here the data was recorded the other way around

df_hot_lap_1 = df_raw_constant_radius[hot_lap_1]
df_hot_lap_2 = df_raw_constant_radius[hot_lap_2]
df_hot_lap_3 = df_raw_constant_radius[hot_lap_3]
df_hot_lap_4 = df_raw_constant_radius[hot_lap_4]
df_hot_lap_5 = df_raw_constant_radius[hot_lap_5]


fig, axs = plt.subplots(5)
axs[0].plot(df_hot_lap_1.loc[:,"Time"], df_hot_lap_1.loc[:,"Steered Angle"])
axs[1].plot(df_hot_lap_2.loc[:,"Time"], df_hot_lap_2.loc[:,"Steered Angle"])
axs[2].plot(df_hot_lap_3.loc[:,"Time"], df_hot_lap_3.loc[:,"Steered Angle"])
axs[3].plot(df_hot_lap_4.loc[:,"Time"], df_hot_lap_4.loc[:,"Steered Angle"])
axs[4].plot(df_hot_lap_5.loc[:,"Time"], df_hot_lap_5.loc[:,"Steered Angle"])


axs[0].legend(['Hot Lap 1'])
axs[1].legend(['Hot Lap 2'])
axs[2].legend(['Hot Lap 3'])
axs[3].legend(['Hot Lap 4'])
axs[3].legend(['Hot Lap 5'])


#PLot the data togheter.
fig5, ax5 = plt.subplots()
ax5.plot(df_hot_lap_1.loc[:,"Time"] - df_hot_lap_1.loc[df_hot_lap_1["Time"].index[0],"Time"], np.abs(df_hot_lap_1.loc[:,"Steered Angle"]))
ax5.plot(df_hot_lap_2.loc[:,"Time"] - df_hot_lap_2.loc[df_hot_lap_2["Time"].index[0],"Time"], np.abs(df_hot_lap_2.loc[:,"Steered Angle"]))
ax5.plot(df_hot_lap_3.loc[:,"Time"] - df_hot_lap_3.loc[df_hot_lap_3["Time"].index[0],"Time"], np.abs(df_hot_lap_3.loc[:,"Steered Angle"]))
ax5.plot(df_hot_lap_4.loc[:,"Time"] - df_hot_lap_4.loc[df_hot_lap_4["Time"].index[0],"Time"], np.abs(df_hot_lap_4.loc[:,"Steered Angle"]))
ax5.plot(df_hot_lap_5.loc[:,"Time"] - df_hot_lap_5.loc[df_hot_lap_5["Time"].index[0],"Time"], np.abs(df_hot_lap_5.loc[:,"Steered Angle"]))


ax5.set(xlabel='time (s)', ylabel='steering angle (deg)', title="Hot Laps")
ax5.legend(['Hot Lap 1',"Hot Lap 2", "Hot Lap 3", "Hot Lap 4","Hot Lap 5"])
ax5.grid()
